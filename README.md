Augmented Reality library and applications
Version 1.0

# About AR
`AR` is Augmented Reality library focusing on tracking and localization. The library will be purely using golang for implementation.

Monday 31st March 2014.
William McCohen

# Todo
- documentation.
- plan for releases.

# Supporting functionalities
## Sensor data (video, kinect, etc) recording and replaying

## AR rendering mechanism with OpenGL

## Panoramic tracking using approach the same as SLAM based with Keyframes


## Planar target tracking

# DEPENDENCIES
- bitbucket.org/williammccohen/cv
- github.com/skelterjohn/go.matrix
- github.com/go-gl/gl
- github.com/go-gl/glfw
