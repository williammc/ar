// Copyright 2014 The Authors. All rights reserved.

package math

import (
	"bitbucket.org/williammccohen/ar/util"
	"bitbucket.org/williammccohen/cv/geometry"
	"fmt"
	matrix "github.com/skelterjohn/go.matrix"
	"runtime"
	"testing"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// Length of normalized vector has to be one.
func TestSE3_ExpLn(t *testing.T) {
	v := matrix.MakeDenseMatrix([]float64{0, 1, 1, 0, 1, 2}, 6, 1)
	p := geometry.Vector{*v}
	p_est := SE3Exp(&p).Ln()
	ok := true
	for i := 0; i < 6; i++ {
		ok = ok && util.Check_CloseEqual(p_est.Get(i), p.Get(i))
	}
	if !ok {
		fmt.Printf("Real Rotation: (%f, %f, %f) Estimate Rotation: (%f, %f, %f)",
			p.Get(3), p.Get(4), p.Get(5), p_est.Get(3), p_est.Get(4), p_est.Get(5))

		fmt.Printf("Real Rotation: (%f, %f, %f) Estimate Rotation: (%f, %f, %f)",
			p.Get(0), p.Get(1), p.Get(2), p_est.Get(0), p_est.Get(1), p_est.Get(2))
		t.Fail()
	}
}
