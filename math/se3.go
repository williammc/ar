// Copyright 2014 The Authors. All rights reserved.

package math

import (
	"bitbucket.org/williammccohen/cv/geometry"
	matrix "github.com/skelterjohn/go.matrix"
	"math"
)

type SE3 struct {
	rotation    SO3
	translation geometry.Vector3d
}

func MakeSE3(rot SO3, t geometry.Vector3d) (res *SE3) {
	res = &SE3{rot, t}
	return
}

// construct from a vector
// resulting SE3 is a rotation around rotation axis v6(1:3)
// with angle=norm(v6(1:3)) and translation v6(4:6)
func MakeSE3_Vector(v6 *geometry.Vector) (trans *SE3) {
	return SE3Exp(v6)
}

// return 3x4 transformation matrix
func (trans *SE3) GetMatrix() (m *matrix.DenseMatrix) {
	m = matrix.Zeros(3, 4)
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			m.Set(i, j, trans.rotation.matrix.Get(i, j))
		}
		m.Set(i, 3, trans.translation.Get(i))
	}
	return
}

func (trans *SE3) GetRotation() *SO3 {
	return &trans.rotation
}

func (trans *SE3) GetTranslation() *geometry.Vector3d {
	return &trans.translation
}

// take logarithm of SE3 and result vector (v6) (inversec step of exp())
func (trans *SE3) Ln() (result *geometry.Vector) {
	rot_temp := trans.rotation
	rv := rot_temp.Ln()
	theta := rv.Norm()
	shtot := 0.5
	if theta > 1.e-6 {
		shtot = math.Sin(theta/2.0) / theta
	}
	v3_rot := matrix.Scaled(&rv.DenseMatrix, -0.5)
	half_rotator := SO3Exp(&geometry.Vector3d{geometry.Vector{*v3_rot}})
	rot_trans := half_rotator.TimesDense(&trans.translation.DenseMatrix)
	if theta > 1.e-3 {
		t1 := (1 - 2*shtot) / rv.SquaredNorm()
		t2, _ := trans.translation.Transpose().TimesDense(&rv.DenseMatrix)
		t3, _ := rv.TimesDense(t2)
		t4 := matrix.Scaled(t3, t1)
		rot_trans, _ = rot_trans.MinusDense(t4)
	} else {
		t1, _ := trans.translation.Transpose().TimesDense(&rv.DenseMatrix)
		t2, _ := rv.TimesDense(t1)
		rot_trans, _ = rot_trans.MinusDense(matrix.Scaled(t2, 1.0/24.0))
	}
	rot_trans = matrix.Scaled(rot_trans, 1.0/2/shtot)
	result = geometry.MakeVector(6)
	for i := 0; i < 3; i++ {
		result.Set(i, rot_trans.Get(i, 0))
		result.Set(3+i, rv.Get(i))
	}
	return
}

// inverse transformation
func (trans *SE3) Inverse() (inv_trans *SE3) {
	inv_trans = new(SE3)
	inv_trans.rotation = *trans.rotation.Inverse()
	t := inv_trans.rotation.TimesDense(&trans.translation.DenseMatrix)
	t = matrix.Scaled(t, -1.0)
	inv_trans.translation = *geometry.MakeVector3d(t.Get(0, 0), t.Get(1, 0), t.Get(2, 0))
	return
}

// exponentiate vector (w) to form SE3 in Lie algebra
// resulting SE3 is rotation around rotation axis w(1:3) with angle theta =
// norm(w(1:3)) and translation w(4:6)
func SE3Exp(mu *geometry.Vector) (se3Res *SE3) {
	one_6th := 1.0 / 6.0
	one_20th := 1.0 / 20.0

	w := *mu.Slice(3, 3)
	theta_sq := w.SquaredNorm()
	theta := math.Sqrt(theta_sq)
	// Use a Taylor series expansion near zero. This is required for
	// accuracy, since sin t / t and (1-cos t)/t^2 are both 0/0.
	cr := w.Cross(mu.Slice(0, 3))
	var A, B, C float64
	var v3Res *matrix.DenseMatrix
	if theta_sq < 1e-8 {
		A = 1.0 - one_6th*theta_sq
		B = 0.5
		v3Res, _ = mu.Slice(0, 3).PlusDense(matrix.Scaled(&cr.DenseMatrix, 0.5))
	} else {
		if theta_sq < 1e-6 {
			B = 0.5 - 0.25*one_6th*theta_sq
			A = 1.0 - theta_sq*one_6th*(1.0-one_20th*theta_sq)
			C = one_6th * (1. - one_20th*theta_sq)
		} else {
			inv_theta := 1.0 / theta
			A = math.Sin(theta) * inv_theta
			B = (1 - math.Cos(theta)) * (inv_theta * inv_theta)
			C = (1 - A) * (inv_theta * inv_theta)
		}
		v3Res, _ = mu.Slice(0, 3).PlusDense(matrix.Scaled(&cr.DenseMatrix, B))
		v3Res, _ = v3Res.PlusDense(matrix.Scaled(&w.Cross(&cr.Vector).DenseMatrix, C))
	}

	se3Res = new(SE3)
	se3Res.rotation.matrix = *rodriguesSO3Exp(&geometry.Vector3d{w}, A, B)
	se3Res.translation.DenseMatrix = *v3Res
	return
}

// Returns the i-th generator times pos.
func SE3GeneratorField(i int, pos *geometry.Vector3d) (result *geometry.Vector) {
	result = new(geometry.Vector)
	result.DenseMatrix = *matrix.Zeros(3, 1)
	if i < 4 {
		result.Set(i, 1)
		return
	}
	result.Set((i%3)+1, -pos.Get(((i+1)%3)+1))
	result.Set(((i+1)%3)+1, pos.Get((i%3)+1))
	return
}

// derivatives of SE3*v3 w.r.t SE3
func DerivSE3(v3 *geometry.Vector3d) (deriv *matrix.DenseMatrix) {
	deriv = matrix.Zeros(3, 6)
	var gfs []*geometry.Vector
	for i := 0; i < 6; i++ {
		gfs[i] = SE3GeneratorField(i, v3)
	}
	for i := 0; i < 3; i++ {
		for j := 0; j < 6; j++ {
			deriv.Set(i, j, gfs[j].Get(i))
		}
	}
	return
}

// ========================== Arithmatic ===================================
func (trans *SE3) Times(a_trans *SE3) (res *SE3) {
	res = new(SE3)
	res.rotation = *trans.rotation.Times(&a_trans.rotation)
	t, _ := trans.translation.PlusDense(trans.rotation.TimesDense(&a_trans.translation.DenseMatrix))
	res.translation = geometry.Vector3d{geometry.Vector{*t}}
	return
}

func (trans *SE3) TimesSE3(a_rot *SO3) (res *SE3) {
	res = new(SE3)
	res.rotation = *trans.rotation.Times(a_rot)
	res.translation = trans.translation
	return
}

func (trans *SE3) TimesDense(m *matrix.DenseMatrix) (res *matrix.DenseMatrix) {
	res = matrix.Zeros(m.Rows(), m.Cols())
	m33 := m.GetMatrix(0, 0, 3, m.Cols())
	if m.Rows() == 4 {
		res.SetMatrix(0, 0, trans.rotation.TimesDense(m33))
		res.SetMatrix(3, 0, m.GetMatrix(3, 0, 1, m.Cols()))
	} else {
		t, _ := trans.rotation.TimesDense(m33).PlusDense(&trans.translation.DenseMatrix)
		res.SetMatrix(0, 0, t)
	}

	return
}
