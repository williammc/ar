// Copyright 2014 The Authors. All rights reserved.

package math

import (
	"bitbucket.org/williammccohen/cv/geometry"
	matrix "github.com/skelterjohn/go.matrix"
	"math"
)

type SO3 struct {
	matrix matrix.DenseMatrix
}

// construct from a vector
// resulting SO3 is a rotation around rotation axis v3
// with angle=norm(v3)
func MakeSO3(v *geometry.Vector3d) (rot *SO3) {
	rot = SO3Exp(v)
	return
}

func (rot *SO3) GetMatrix() *matrix.DenseMatrix {
	return &rot.matrix
}

func (rot *SO3) SetMatrix(m *matrix.DenseMatrix) bool {
	if m.Det() == 1.0 {
		rot.matrix = *m
		return true
	}
	return false
}

func (rot *SO3) Ln() (result *geometry.Vector3d) {
	cos_angle := (rot.matrix.Get(0, 0) + rot.matrix.Get(1, 1) + rot.matrix.Get(2, 2) - 1.0) * 0.5
	result = geometry.MakeVector3d(0, 0, 0)
	result.Set(0, (rot.matrix.Get(2, 1)-rot.matrix.Get(1, 2))/2)
	result.Set(1, (rot.matrix.Get(0, 2)-rot.matrix.Get(2, 0))/2)
	result.Set(2, (rot.matrix.Get(1, 0)-rot.matrix.Get(0, 1))/2)
	sin_angle_abs := result.Norm()
	if cos_angle > math.Cos(math.Pi/4) { // [0 - Pi/4] use asin
		if sin_angle_abs > 0 {
			result.DenseMatrix = *matrix.Scaled(&result.DenseMatrix, math.Asin(sin_angle_abs)/sin_angle_abs)
		}
	} else if cos_angle > -math.Cos(math.Pi/4) { // [Pi/4 - 3Pi/4[ use acos, but antisymmetric part
		angle := math.Acos(cos_angle)
		result.DenseMatrix = *matrix.Scaled(&result.DenseMatrix, angle/sin_angle_abs)
	} else { // rest use symmetric part
		// antisymmetric part vanishes, but still large rotation
		// need information from symmetric part
		angle := math.Pi - math.Asin(sin_angle_abs)
		d0 := rot.matrix.Get(0, 0) - cos_angle
		d1 := rot.matrix.Get(1, 1) - cos_angle
		d2 := rot.matrix.Get(2, 2) - cos_angle

		r2 := geometry.MakeVector(3)
		if d0*d0 > d1*d1 && d0*d0 > d2*d2 { // first is largest, fill with first column
			r2.Set(0, d0)
			r2.Set(1, (rot.matrix.Get(1, 0)+rot.matrix.Get(0, 1))/2)
			r2.Set(2, (rot.matrix.Get(0, 2)+rot.matrix.Get(2, 0))/2)
		} else if d1*d1 > d2*d2 { // second is largest, fill with second column
			r2.Set(0, (rot.matrix.Get(1, 0)+rot.matrix.Get(0, 1))/2)
			r2.Set(1, d1)
			r2.Set(2, (rot.matrix.Get(2, 1)+rot.matrix.Get(1, 2))/2)
		} else { // third is largest, fill with third column
			r2.Set(0, (rot.matrix.Get(0, 2)+rot.matrix.Get(2, 0))/2)
			r2.Set(1, (rot.matrix.Get(2, 1)+rot.matrix.Get(1, 0))/2)
			r2.Set(2, d2)
		}
		// flip, if we point in the wrong direction!
		if t, _ := r2.Transpose().TimesDense(&result.DenseMatrix); t.Get(0, 0) < 0 {
			r2.DenseMatrix = *matrix.Scaled(&r2.DenseMatrix, -1)
		}
		result.DenseMatrix = *matrix.Scaled(&r2.Normalized().DenseMatrix, angle)
	}
	return
}

// Get inverse rotation.
func (rot *SO3) Inverse() (inv_so3 *SO3) {
	inv_so3 = new(SO3)
	inv_so3.matrix = *rot.matrix.Transpose()
	return
}

// exponentiate vector (w) to form SO3 in Lie algebra
// resulting SO3 is rotation around rotation axis w with angle theta =
// norm(w)
func SO3Exp(w *geometry.Vector3d) (rot *SO3) {
	rot = new(SO3)
	one_6th := 1.0 / 6.0
	one_20th := 1.0 / 20.0

	theta_sq := w.SquaredNorm()
	theta := math.Sqrt(theta_sq)
	// Use a Taylor series expansion near zero. This is required for
	// accuracy, since sin t / t and (1-cos t)/t^2 are both 0/0.
	var A, B float64
	if theta_sq < 1e-8 {
		A = 1.0 - one_6th*theta_sq
		B = 0.5
	} else {
		if theta_sq < 1e-6 {
			B = 0.5 - 0.25*one_6th*theta_sq
			A = 1.0 - theta_sq*one_6th*(1.0-one_20th*theta_sq)
		} else {
			inv_theta := 1.0 / theta
			A = math.Sin(theta) * inv_theta
			B = (1.0 - math.Cos(theta)) * (inv_theta * inv_theta)

		}
	}
	rot.matrix = *rodriguesSO3Exp(w, A, B)
	return
}

// compute a rotation exponential using the Rodrigues Formula
// INPUT
// w: rotation axis
// A: sin(theta)/theta
// B: (1-cos(theta))/theta^2
// OUTPUT
// R: rotation matrix
func rodriguesSO3Exp(w *geometry.Vector3d, A, B float64) (R *matrix.DenseMatrix) {
	wx2 := w.Get(0) * w.Get(0)
	wy2 := w.Get(1) * w.Get(1)
	wz2 := w.Get(2) * w.Get(2)

	R = matrix.Zeros(3, 3)
	R.Set(0, 0, 1.0-B*(wy2+wz2))
	R.Set(1, 1, 1.0-B*(wx2+wz2))
	R.Set(2, 2, 1.0-B*(wx2+wy2))

	a := A * w.Get(2)
	b := B * (w.Get(0) * w.Get(1))
	R.Set(0, 1, b-a)
	R.Set(1, 0, b+a)

	a = A * w.Get(1)
	b = B * (w.Get(0) * w.Get(2))
	R.Set(0, 2, b+a)
	R.Set(2, 0, b-a)

	a = A * w.Get(0)
	b = B * (w.Get(1) * w.Get(2))
	R.Set(1, 2, b-a)
	R.Set(2, 1, b+a)
	return
}

// Returns the i-th generator times pos
func SO3GeneratorField(i int, pos *geometry.Vector3d) (result *geometry.Vector3d) {
	result = geometry.MakeVector3d(0, 0, 0)
	result.Set(i%3+1, -pos.Get((i+1)%3+1))
	result.Set((i+1)%3+1, pos.Get(i%3+1))
	return
}

// calc derivatives for SO3*v3 w.r.t SO3
func DerivSO3(v3 *geometry.Vector3d) (deriv *matrix.DenseMatrix) {
	deriv = matrix.Zeros(3, 3)
	gf1 := SO3GeneratorField(1, v3)
	gf2 := SO3GeneratorField(2, v3)
	gf3 := SO3GeneratorField(3, v3)
	for i := 0; i < 3; i++ {
		deriv.Set(i, 1, gf1.Get(i))
		deriv.Set(i, 2, gf2.Get(i))
		deriv.Set(i, 3, gf3.Get(i))
	}
	return
}

// ========================== Arithmatic ===================================
func (rot *SO3) Times(a_rot *SO3) (res *SO3) {
	res = new(SO3)
	t, _ := rot.matrix.TimesDense(&a_rot.matrix)
	res.matrix = *t
	return
}

func (rot *SO3) TimesSE3(a_trans *SE3) (res *SE3) {
	res = new(SE3)
	res.rotation = *rot.Times(&a_trans.rotation)
	res.translation.DenseMatrix = *rot.TimesDense(&a_trans.translation.DenseMatrix)
	return
}

func (rot *SO3) TimesDense(m *matrix.DenseMatrix) (res *matrix.DenseMatrix) {
	res, _ = rot.matrix.TimesDense(m)
	return
}
