// Copyright 2014 The Authors. All rights reserved.

package math

import (
	matrix "github.com/skelterjohn/go.matrix"
	"log"
	"math"
)

// Assume given a Vector (matrix Nx1).
func Norm(m *matrix.DenseMatrix) (res float64) {
	res = 0.0
	if m.Cols() == 1 {
		for i := 0; i < m.Rows(); i++ {
			res += m.Get(i, 0) * m.Get(i, 0)
		}
		res = math.Sqrt(res)
		return
	} else if m.Rows() == 1 {
		for i := 0; i < m.Cols(); i++ {
			res += m.Get(0, i) * m.Get(0, i)
		}
		res = math.Sqrt(res)
		return
	}

	log.Fatal("Norm:: given matrix is not a vector like.")
	return
}

// Assume given a Vector (matrix Nx1).
func SquaredNorm(m *matrix.DenseMatrix) (res float64) {
	res = 0.0
	if m.Cols() == 1 {
		for i := 0; i < m.Rows(); i++ {
			res += m.Get(i, 0) * m.Get(i, 0)
		}
		return
	} else if m.Rows() == 1 {
		for i := 0; i < m.Cols(); i++ {
			res += m.Get(0, i) * m.Get(0, i)
		}
		return
	}

	log.Fatal("SquaredNorm:: given matrix is not a vector like.")
	return
}
