// Copyright 2014 The Authors. All rights reserved.

package math

import (
	"bitbucket.org/williammccohen/ar/util"
	"bitbucket.org/williammccohen/cv/geometry"
	"fmt"
	"runtime"
	"testing"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// Length of normalized vector has to be one.
func TestSO3_ExpLn(t *testing.T) {
	p := geometry.MakeVector3d(0, 1, 2)
	p_est := SO3Exp(p).Ln()
	if !util.Check_CloseEqual(p_est.Get(0), p.Get(0)) ||
		!util.Check_CloseEqual(p_est.Get(1), p.Get(1)) ||
		!util.Check_CloseEqual(p_est.Get(2), p.Get(2)) {
		fmt.Printf("Real: (%f, %f, %f) Estimate: (%f, %f, %f)",
			p.Get(0), p.Get(1), p.Get(2), p_est.Get(0), p_est.Get(1), p_est.Get(2))
		t.Fail()
	}
}
