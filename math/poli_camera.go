// Copyright 2014 The Authors. All rights reserved.

// I'm a camera man, in mathematical aspect.
// You can know me more through description below.
// Welcome comments about camera's models & calibration techniques.

package math

import (
	"bitbucket.org/williammccohen/cv/geometry" // use Vector2d
	matrix "github.com/skelterjohn/go.matrix"
)

// What is a camera in mathematical aspect?
type CameraModel interface {
	Project(p *geometry.Vector2d) *geometry.Vector2d
	UnProject(p *geometry.Vector2d) *geometry.Vector2d
	GetProjectionDerivatives(p *geometry.Vector2d) *matrix.DenseMatrix

	ProjectLinear(p_cam *geometry.Vector2d) *geometry.Vector2d
	UnProjectLinear(p_cam *geometry.Vector2d) *geometry.Vector2d

	SetParameters(p []float64)
	Parameters() []float64

	GetResolution() (int, int)
}

// Polynomial Camera model is Pin Hole projection model
// applying Taylor series for radial distortion
// (u,v) = ProjectionMatrix*(R_/R)*(x,y)
// Pixel coordinate (u,v) start from Left-Top corner,
// with vector u is along hirozontal left-right, vector v is vertical top-down
// Responsibilities: Projecttion(3D to 2D) &
// Unprojection(2D to 3D, at camera plane where Zc=1.)
type PoliCamera struct {
	params []float64 // fx, fy, cx, cy, k1, k2
	width  int
	height int
}

func MakePoliCamera(width, height int) (cam *PoliCamera) {
	cam = new(PoliCamera)
	cam.params = []float64{640, 480, 320, 240, 0, 0}
	cam.width = width
	cam.height = height
	return
}

// Projection from camera plane (z=1) to image plane.
func (cam *PoliCamera) Project(p_cam *geometry.Vector2d) *geometry.Vector2d {
	r2 := p_cam.Get(0)*p_cam.Get(0) + p_cam.Get(1)*p_cam.Get(1)
	r4 := r2 * r2
	factor := 1 + cam.params[4]*r2 + cam.params[5]*r4
	t1 := cam.params[0]*p_cam.Get(0)*factor + cam.params[2]
	t2 := cam.params[1]*p_cam.Get(1)*factor + cam.params[3]
	p_im := geometry.MakeVector2d(t1, t2)
	return p_im
}

// UnProjection from image plane to camera plane (z=1).
func (cam *PoliCamera) UnProject(p_im *geometry.Vector2d) *geometry.Vector2d {
	t1 := (p_im.Get(0) - cam.params[2]) / cam.params[0]
	t2 := (p_im.Get(1) - cam.params[3]) / cam.params[1]
	p_cam := geometry.MakeVector2d(t1, t2)
	scale := p_cam.Get(0)*p_cam.Get(0) + p_cam.Get(1)*p_cam.Get(1)
	// 3 Newton-Raphson iterations are good enough in certain range
	for i := 0; i < 3; i++ {
		t := 1 + scale*(cam.params[4]+scale*cam.params[5])
		err := p_cam.SquaredNorm() - scale*t*t
		deriv := t * (t + 2*scale*(cam.params[4]+2*scale*cam.params[5]))
		scale += err / deriv
	}
	t := (1 + scale*(cam.params[4]+scale*cam.params[5]))
	p_cam.Set(0, p_cam.Get(0)/t)
	p_cam.Set(1, p_cam.Get(1)/t)
	return p_cam
}

// Linear part of projection.
func (cam *PoliCamera) ProjectLinear(p_cam *geometry.Vector2d) *geometry.Vector2d {
	t1 := cam.params[0]*p_cam.Get(0) + cam.params[2]
	t2 := cam.params[1]*p_cam.Get(1) + cam.params[3]
	p_im := geometry.MakeVector2d(t1, t2)
	return p_im
}

// Linear part of unprojection.
func (cam *PoliCamera) UnProjectLinear(p_im *geometry.Vector2d) *geometry.Vector2d {
	t1 := (p_im.Get(0) - cam.params[2]) / cam.params[0]
	t2 := (p_im.Get(1) - cam.params[3]) / cam.params[1]
	p_cam := geometry.MakeVector2d(t1, t2)
	return p_cam
}

func (cam *PoliCamera) SetParameters(p []float64) {
	copy(cam.params, p)
}

func (cam *PoliCamera) Parameters() []float64 {
	return cam.params
}

func (cam *PoliCamera) GetProjectionDerivatives(p_cam *geometry.Vector2d) *matrix.DenseMatrix {
	deriv := matrix.Eye(2)
	t1 := p_cam.SquaredNorm()
	t2 := cam.params[5] * t1
	deriv = matrix.Scaled(deriv, 1+t1*(cam.params[4]+t2))
	t := matrix.Scaled(&p_cam.DenseMatrix, 2*(cam.params[4]+2*t2))
	t, _ = t.TimesDense(t.Transpose())
	_ = deriv.Add(t)
	deriv.SetMatrix(0, 0, matrix.Scaled(deriv.GetRowVector(0), cam.params[0]))
	deriv.SetMatrix(1, 0, matrix.Scaled(deriv.GetRowVector(1), cam.params[2]))
	return deriv
}

func (cam *PoliCamera) GetResolution() (int, int) {
	return cam.width, cam.height
}
