// Copyright 2014 The Authors. All rights reserved.

package math

import (
	"bitbucket.org/williammccohen/ar/util"
	"bitbucket.org/williammccohen/cv/geometry"
	"runtime"
	"testing"
)

var point_im = geometry.MakeVector2d(200, 100)
var cam = MakePoliCamera(640, 480)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	params := []float64{640, 480, 320, 240, 0.3, -0.01}
	cam.SetParameters(params)
}

// Length of normalized vector has to be one.
func TestPoliCamera_UnprojectAndProject(t *testing.T) {
	point_est := cam.Project(cam.UnProject(point_im))
	if !util.Check_CloseEqual(point_est.Get(0), point_im.Get(0)) ||
		!util.Check_CloseEqual(point_est.Get(1), point_im.Get(1)) {
		t.Fail()
	}
}

// Length of normalized vector has to be one.
func TestPoliCamera_UnprojectLinearAndProjectLinear(t *testing.T) {
	point_est := cam.ProjectLinear(cam.UnProjectLinear(point_im))
	if !util.Check_CloseEqual(point_est.Get(0), point_im.Get(0)) ||
		!util.Check_CloseEqual(point_est.Get(1), point_im.Get(1)) {
		t.Fail()
	}
}
