// Copyright 2014 The Authors. All rights reserved.

// NEHE Tutorial 08:  Blending.
// http://nehe.gamedev.net/data/lessons/lesson.asp?lesson=08
package main

import (
	goar_math "bitbucket.org/williammccohen/ar/math"
	"bitbucket.org/williammccohen/ar/ui"
	"bitbucket.org/williammccohen/cv/imgproc"
	"fmt"
	"github.com/go-gl/gl"
	"github.com/go-gl/glfw"
	"image"
	"image/jpeg"
	"log"
	"os"
)

const (
	Title  = "Simple AR"
	Width  = 640
	Height = 480
)

var (
	glwindow     *ui.GLWindow
	filename     string
	running      bool
	textures     []gl.Texture = make([]gl.Texture, 3)
	texturefiles [1]string
	light        bool                                     // Display light?
	blend        bool                                     // Perform blending?
	rotation     [2]float32                               // X/Y rotation.
	speed        [2]float32                               // X/Y speed.
	z            float32    = -5                          // Depth into the scene.
	ambient      []float32  = []float32{0.5, 0.5, 0.5, 1} // ambient light colour.
	diffuse      []float32  = []float32{1, 1, 1, 1}       // diffuse light colour.
	lightpos     []float32  = []float32{0, 0, 2, 1}       // Position of light source.
	filter       int                                      // Index of current texture to display.
)

func main() {
	var err error
	if err = glfw.Init(); err != nil {
		log.Fatalf("%v\n", err)
		return
	}

	defer glfw.Terminate()

	if err = glfw.OpenWindow(Width, Height, 8, 8, 8, 8, 8, 0, glfw.Windowed); err != nil {
		log.Fatalf("%v\n", err)
		return
	}

	defer glfw.CloseWindow()

	// open "test.jpg"
	file, err := os.Open("/home/thanh/data/goar/family.jpg")
	if err != nil {
		log.Fatal(err)
	}

	// decode jpeg into image.Image
	img, err := jpeg.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()

	fmt.Print("Loaded image type:")
	switch img.(type) {
	case *image.YCbCr:
		fmt.Println("*image.YCbCr")
	case *image.Gray:
		fmt.Println("*image.Gray")
	default:
		fmt.Println("Donnot know type")
	}

	sd := new(SimpleDrawable)
	sc := new(SimpleCallback)
	sc.img = imgproc.ConvertImageToRGBA(img)

	cam := goar_math.MakePoliCamera(img.Bounds().Dx(), img.Bounds().Dy())
	w, h := float64(img.Bounds().Dx()), float64(img.Bounds().Dy())
	params := []float64{w, w, w / 2, h / 2, 0.3, -0.01}
	cam.SetParameters(params)

	ar := ui.NewARRender()
	defer ar.Release()

	ar.AddDrawable(sd)
	ar.Configure(cam)
	ar.Init()

	glwindow = ui.NewGLWindow(ar, sc)

	glfw.SetSwapInterval(1)
	glfw.SetWindowTitle(Title)
	// glfw.SetWindowSizeCallback(reshape)
	// glfw.SetKeyCallback(on_keyboard)

	glwindow.Main()
}

func reshape(w, h int) {
	glwindow.Reshape(w, h)
}

func on_keyboard(key, state int) {
	glwindow.OnKeyboard(key)
}

type SimpleDrawable struct {
}

func (sd *SimpleDrawable) Draw() {
	gl.Begin(gl.QUADS)
	// Front Face
	gl.Normal3f(0, 0, 1) // Normal Pointing Towards Viewer
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(-1, -1, 1) // Point 1 (Front)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(1, -1, 1) // Point 2 (Front)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(1, 1, 1) // Point 3 (Front)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(-1, 1, 1) // Point 4 (Front)
	// Back Face
	gl.Normal3f(0, 0, -1) // Normal Pointing Away From Viewer
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(-1, -1, -1) // Point 1 (Back)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(-1, 1, -1) // Point 2 (Back)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(1, 1, -1) // Point 3 (Back)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(1, -1, -1) // Point 4 (Back)
	// Top Face
	gl.Normal3f(0, 1, 0) // Normal Pointing Up
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(-1, 1, -1) // Point 1 (Top)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(-1, 1, 1) // Point 2 (Top)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(1, 1, 1) // Point 3 (Top)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(1, 1, -1) // Point 4 (Top)
	// Bottom Face
	gl.Normal3f(0, -1, 0) // Normal Pointing Down
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(-1, -1, -1) // Point 1 (Bottom)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(1, -1, -1) // Point 2 (Bottom)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(1, -1, 1) // Point 3 (Bottom)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(-1, -1, 1) // Point 4 (Bottom)
	// Right face
	gl.Normal3f(1, 0, 0) // Normal Pointing Right
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(1, -1, -1) // Point 1 (Right)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(1, 1, -1) // Point 2 (Right)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(1, 1, 1) // Point 3 (Right)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(1, -1, 1) // Point 4 (Right)
	// Left Face
	gl.Normal3f(-1, 0, 0) // Normal Pointing Left
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(-1, -1, -1) // Point 1 (Left)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(-1, -1, 1) // Point 2 (Left)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(-1, 1, 1) // Point 3 (Left)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(-1, 1, -1)
	gl.End()
}

func (sd *SimpleDrawable) Draw2D() {

}

type SimpleCallback struct {
	img *image.RGBA
}

func (sc *SimpleCallback) GetVideoFrame() []uint8 {
	return sc.img.Pix
}

func (sc *SimpleCallback) OnDisplay() {

}

func (sc *SimpleCallback) OnKeyboard(key int) {
	fmt.Println("OnKeyboard")

}
