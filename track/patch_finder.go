// Copyright 2014 The Authors. All rights reserved.

package track

import (
	"bitbucket.org/williammccohen/ar/util"
	"bitbucket.org/williammccohen/cv/geometry"
	"bitbucket.org/williammccohen/cv/imgproc"
	"image"
	"log"
	"math"
)

// Patch finder.
type PatchFinder struct {
	patch_size          int
	search_radius       int
	search_window_size  int
	img                 *image.Gray
	ncc                 *imgproc.Float64Image
	sum_intensity       *imgproc.Uint32Image
	sumsq_intensity     *imgproc.Uint64Image
	use_subpixel_search bool
	last_result         geometry.Vector2d
}

func NewPatchFinder(patch_size, radius int, use_sub bool) *PatchFinder {
	patchfinder := new(PatchFinder)
	patchfinder.patch_size = patch_size
	patchfinder.search_radius = radius
	patchfinder.use_subpixel_search = use_sub

	ws := 2*radius + 1 + patch_size
	patchfinder.search_window_size = ws
	patchfinder.ncc = imgproc.NewFloat64Image(image.Rect(0, 0, 2*radius+2, 2*radius+2))
	patchfinder.sum_intensity = imgproc.NewUint32Image(image.Rect(0, 0, ws+1, ws+1))
	patchfinder.sumsq_intensity = imgproc.NewUint64Image(image.Rect(0, 0, ws+1, ws+1))

	return patchfinder
}

// Reference to external image.
func (pf *PatchFinder) SetSearchImage(img *image.Gray) {
	pf.img = img
}

func (pf *PatchFinder) CheckLocation(location *geometry.Vector2d) bool {
	return util.InImageWithBorder(pf.img.Rect, image.Pt(int(location.Get(0)), int(location.Get(1))), pf.search_radius)
}

func integral_search(patch *image.Gray, I *image.Gray, sumA uint, nA int,
	sumI *imgproc.Uint32Image, sumsqI *imgproc.Uint64Image,
	ncc *imgproc.Float64Image) (*geometry.Vector2d, float64) {
	s1 := sumI.SubImage(image.Rect(1, 1, I.Bounds().Dx(), I.Bounds().Dy()))
	s2 := sumsqI.SubImage(image.Rect(1, 1, I.Bounds().Dx(), I.Bounds().Dy()))
	imgproc.IntegralInPlace(I, s1, s2)
	best_loc, best_score := geometry.MakeVector2d(0, 0), -1.0
	apatch := imgproc.NewGrayAccessor(patch)
	for y := 0; y < I.Bounds().Dy()-patch.Bounds().Dy(); y++ {
		for x := 0; x < I.Bounds().Dx()-patch.Bounds().Dx(); x++ {
			p := geometry.MakeVector2d(float64(x), float64(y))
			w := image.NewGray(image.Rect(x, y, patch.Bounds().Dx(), patch.Bounds().Dy()))
			aw := imgproc.NewGrayAccessor(w)
			dot_value := dot(apatch, aw)
			p1 := geometry.MakeVector2d(float64(x+patch.Bounds().Dx()), float64(y+patch.Bounds().Dy()))
			score := NCC_score_64(sumA, nA, calc_rect(sumI, p, p1), calc_rect(sumsqI, p, p1), dot_value)
			ncc.Set(x, y, score)
			if score > best_score {
				best_loc = p
				best_score = score
			}
		}
	}
	return best_loc, best_score
}

func NCC_score_64(sumA uint, nA int, sumB uint, sumsqB uint, dot uint) float64 {
	nB := (64*int(sumsqB) - int(sumB)*int(sumB))
	nom := 64*int(dot) - int(sumA)*int(sumB)

	if nA == 0 || nB == 0 {
		return -1.0
	}

	result := ((math.Abs(float64(nom))) * float64(nom)) / (float64(nA) * float64(nB))
	return result
}

func calc_rect(img imgproc.Image, from, to *geometry.Vector2d) uint {
	t := imgproc.CalcRect(img, image.Pt(int(from.Get(0)), int(from.Get(1))),
		image.Pt(int(to.Get(0)), int(to.Get(1))))
	return uint(t)
}

// Calculate moments of image.
func calculate_moments(I *imgproc.GrayAcessor) (uint, uint) {
	sum := uint(0)
	sumsq := uint(0)
	for y := 0; y < I.Src().(image.Image).Bounds().Dy(); y++ {
		for x := 0; x < I.Src().(image.Image).Bounds().Dx(); x++ {
			sum += uint(*I.Uint8At(x, y, 0))
			sumsq += uint(*I.Uint8At(x, y, 0)) * uint(*I.Uint8At(x, y, 0))
		}
	}
	return sum, sumsq
}

// Slow direct implementation for dot product.
func dot(A, B *imgproc.GrayAcessor) uint {
	if !A.Src().(image.Gray).Rect.Eq(B.Src().(image.Gray).Rect) {
		log.Fatal("PatchFinder:: dot(A, B) A.size != B.size")
	}
	dot := uint(0)
	for y := 0; y < A.Src().(image.Image).Bounds().Dy(); y++ {
		for x := 0; x < A.Src().(image.Image).Bounds().Dx(); x++ {
			dot += uint(*A.Uint8At(x, y, 0) * *B.Uint8At(x, y, 0))
		}
	}
	return dot
}

// Search the patch.
func (pf *PatchFinder) Search(patch *image.Gray, location *geometry.Vector2d) (*geometry.Vector2d, float64) {
	var sum, sumsq uint
	apatch := imgproc.NewGrayAccessor(patch)
	sum, sumsq = calculate_moments(apatch)
	nominator := 64*int(sumsq) - int(sum)*int(sum)

	from, _ := location.MinusDense(
		&geometry.MakeVector2d(float64(
			pf.search_window_size/2), float64(pf.search_window_size/2)).DenseMatrix)
	m_roi := pf.img.SubImage(image.Rect(int(from.Get(0, 0)), int(from.Get(1, 0)), pf.search_window_size, pf.search_window_size))
	loc, score := integral_search(patch, m_roi.(*image.Gray), sum, nominator, pf.sum_intensity, pf.sumsq_intensity, pf.ncc)
	pf.last_result = *loc

	t, _ := from.PlusDense(&geometry.MakeVector2d(float64(pf.patch_size)/2, float64(pf.patch_size)/2).DenseMatrix)
	_ = loc.AddDense(t)

	if pf.use_subpixel_search {
		sub_loc, sub_score := pf.sub_pixel_location()
		sub_loc.AddDense(&loc.DenseMatrix)
		return sub_loc, sub_score
	}
	return loc, score
}

// Sub pixel interpolation.
func (pf *PatchFinder) sub_pixel_location() (*geometry.Vector2d, float64) {
	p := image.Pt(int(pf.last_result.Get(0)), int(pf.last_result.Get(1)))
	if util.InImageWithBorder(pf.ncc.Rect, p, 1) {
		loc, score := util.InterpolateExtremumValue(pf.ncc, &pf.last_result)
		diff, _ := loc.MinusDense(&pf.last_result.DenseMatrix)

		v := geometry.Vector2d{geometry.Vector{*diff}}
		if v.InfNorm() <= 1.0 &&
			score > pf.ncc.At(int(pf.last_result.Get(0)), int(pf.last_result.Get(1))) &&
			score <= 1.0 {
			return &v, score
		}
	}

	return geometry.MakeVector2d(0.0, 0.0), pf.ncc.At(p.X, p.Y)
}
