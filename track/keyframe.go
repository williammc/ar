// Copyright 2014 The Authors. All rights reserved.

package track

import (
	ar_math "bitbucket.org/williammccohen/ar/math"
	"bitbucket.org/williammccohen/ar/util"
	"bitbucket.org/williammccohen/cv/feature2d"
	"bitbucket.org/williammccohen/cv/geometry"
	"bitbucket.org/williammccohen/cv/imgproc"
	"github.com/nfnt/resize"
	matrix "github.com/skelterjohn/go.matrix"
	"image"
	"math"
)

func scale_from_level(level int) float64 {
	if level > -1 {
		t := (1 << uint(level))
		return float64(t)
	}
	t := (1 << uint(-level))
	return float64(1) / float64(t)
}

func scale_value_by_levels(v float64, level int) float64 {
	return (v+float64(0.5))*scale_from_level(level) - float64(0.5)
}

func scale_by_levels(v *geometry.Vector2d, level int) *geometry.Vector2d {
	return geometry.MakeVector2d(scale_value_by_levels(v.Get(0), level),
		scale_value_by_levels(v.Get(1), level))
}

func intersect_perspective_matrix(plane *geometry.Vector4d) *matrix.DenseMatrix {
	result := matrix.Zeros(4, 3)
	result.SetMatrix(0, 0, matrix.Scaled(matrix.Eye(3), plane.Get(3)))
	result.SetMatrix(3, 0, matrix.Scaled(plane.Slice(0, 3).Transpose(), -1))
	return result
}

func intersect_ortho_matrix(plane *geometry.Vector4d) *matrix.DenseMatrix {
	result := matrix.Zeros(4, 3)
	result.SetMatrix(0, 0, matrix.MakeDenseMatrix([]float64{plane.Get(2), 0, 0}, 1, 3))
	result.SetMatrix(1, 0, matrix.MakeDenseMatrix([]float64{0, plane.Get(2), 0}, 1, 3))
	result.SetMatrix(2, 0, matrix.MakeDenseMatrix(
		[]float64{-plane.Get(0), -plane.Get(1), -plane.Get(3)}, 1, 3))
	result.SetMatrix(3, 0, matrix.MakeDenseMatrix([]float64{0, 0, plane.Get(2)}, 1, 3))

	return result
}

func linearize_homography(H *matrix.DenseMatrix, X *geometry.Vector2d) *matrix.DenseMatrix {
	pr, _ := H.TimesDense(&geometry.UnProject(&X.Vector).DenseMatrix)
	nom := 1.0 / pr.Get(2, 0)
	nom_sq := nom * nom
	result := matrix.Scaled(H.GetMatrix(0, 0, 2, 2), nom)

	t := result.Get(0, 0) - pr.Get(0, 0)*H.Get(2, 0)*nom_sq
	result.Set(0, 0, t)

	t = result.Get(0, 1) - pr.Get(0, 0)*H.Get(2, 1)*nom_sq
	result.Set(0, 0, t)

	t = result.Get(1, 0) - pr.Get(1, 0)*H.Get(2, 0)*nom_sq
	result.Set(0, 0, t)

	t = result.Get(1, 1) - pr.Get(1, 0)*H.Get(2, 1)*nom_sq
	result.Set(0, 0, t)
	return result
}

type KeyframeInterface interface {
	Pose() *ar_math.SE3
	Observations() []Measurement

	Project(*geometry.Vector2d) *geometry.Vector2d
	ProjectLinearize(*geometry.Vector2d) *matrix.DenseMatrix

	Project_WorldPoint(*geometry.Vector4d) *geometry.Vector2d
	ProjectLinearize_WorldPoint(*geometry.Vector4d) *matrix.DenseMatrix

	UnProject(*geometry.Vector2d) *geometry.Vector2d
	UnProjectLinearize(*geometry.Vector2d) *matrix.DenseMatrix

	UnProjectToImgageplane(*geometry.Vector2d) *geometry.Vector4d

	UnProjectLinearize_ImagePoint(*geometry.Vector2d, *geometry.Vector4d) *matrix.DenseMatrix

	UnProjectLinearize_WorldPoint(*geometry.Vector4d, *geometry.Vector4d) *matrix.DenseMatrix

	Sample(*geometry.Vector2d, *matrix.DenseMatrix, *image.Gray) (bool, int)
}

// Abstraction of Keyframe types.
type AbstractKeyframe struct {
	scale_images []*image.Gray // pyramid of images for tracking
	pose         ar_math.SE3   // the pose/rotation of this keyframe
	observations []Measurement
	// indices to visible features in this keyframe
	// (temporal storage used by ModelMaker)
	visiblefeatures []uint

	// mean of scene depths
	// used for limiting epipolar search range (Goerge Klein's idea?!)
	scenedepth_mean  float64
	scenedepth_sigma float64
}

func NewAbstractKeyframe(img *image.Gray, scales int) (ak *AbstractKeyframe) {
	ak = new(AbstractKeyframe)
	ak.scale_images = make([]*image.Gray, scales, scales)
	ak.scale_images[0] = imgproc.NewGrayAccessor(img).Clone().(*image.Gray)
	for i := 1; i < scales; i++ {
		ak.scale_images[i] = resize.Resize(uint(ak.scale_images[i-1].Bounds().Dx()/2),
			uint(ak.scale_images[i-1].Bounds().Dy()/2),
			ak.scale_images[i-1], resize.NearestNeighbor).(*image.Gray)
	}
	return
}

func (ak *AbstractKeyframe) Pose() ar_math.SE3 {
	return ak.pose
}

func (ak *AbstractKeyframe) Observations() []Measurement {
	return ak.observations
}

func (ak *AbstractKeyframe) Scales() int {
	return len(ak.scale_images)
}

func (ak *AbstractKeyframe) Sample(location *geometry.Vector2d, mapping *matrix.DenseMatrix,
	patch *image.Gray) (bool, int) {
	ref_level := 0
	det := mapping.Det()
	for det > 2 && ref_level < ak.Scales()-1 {
		det /= 4
		ref_level += 1
	}
	t := 1 << uint(ref_level)
	mapping = matrix.Scaled(mapping, float64(t))
	location = scale_by_levels(location, -ref_level)
	t1 := math.Max(float64(patch.Bounds().Dx()), float64(patch.Bounds().Dy())) * 0.8
	if !util.InImageWithBorder(ak.scale_images[ref_level].Rect, image.Pt(int(location.Get(0)), int(location.Get(1))),
		int(t1)) {
		return false, ref_level
	}
	asrc := imgproc.NewGrayAccessor(ak.scale_images[ref_level])
	adst := imgproc.NewGrayAccessor(patch)
	dst_ori := geometry.MakeVector2d(float64(patch.Bounds().Dx())*0.5, float64(patch.Bounds().Dy())*0.5)
	outside_pixels := imgproc.Transform(asrc, adst, mapping, &location.DenseMatrix, &dst_ori.DenseMatrix)
	if outside_pixels > 0 {
		return false, ref_level
	}
	return true, ref_level

}

func (ak *AbstractKeyframe) RefreshSceneDepth() {
	scenedepth_mean := 0.0
	scenedepth_sigma := 0.0
	nobs := len(ak.observations)
	if nobs < 2 {
		return
	}
	sum, squared_sum := 0.0, 0.0
	for i := 0; i < nobs; i++ {
		if !ak.observations[i].feature.trashed {
			t := ak.pose.TimesDense(&ak.observations[i].feature.center.DenseMatrix)
			p_cam := geometry.Project(&geometry.Vector{*t})
			sum += p_cam.Get(2)
			squared_sum += p_cam.Get(2) * p_cam.Get(2)
		}
	}
	scenedepth_mean = sum / float64(nobs)
	scenedepth_sigma = math.Sqrt(squared_sum/float64(nobs) -
		scenedepth_mean*scenedepth_mean)
	if scenedepth_sigma == 0.0 {
		scenedepth_sigma = 0.1 * scenedepth_mean
	}
}

func (ak *AbstractKeyframe) Project(*geometry.Vector2d) (res *geometry.Vector2d) {
	return
}

func (ak *AbstractKeyframe) ProjectLinearize(*geometry.Vector2d) (res *geometry.Vector2d) {
	return
}

func (ak *AbstractKeyframe) Project_WorldPoint(*geometry.Vector4d) (res *geometry.Vector2d) {
	return
}

func (ak *AbstractKeyframe) ProjectLinearize_WorldPoint(*geometry.Vector4d) (res *geometry.Vector2d) {
	return
}

func (ak *AbstractKeyframe) UnProject(*geometry.Vector2d) (res *geometry.Vector2d) {
	return
}

func (ak *AbstractKeyframe) UnProjectLinearize(*geometry.Vector2d) (res *geometry.Vector2d) {
	return
}

func (ak *AbstractKeyframe) UnProjectToImgageplane(*geometry.Vector2d) (res *geometry.Vector4d) {
	return
}

func (ak *AbstractKeyframe) UnProjectLinearize_ImagePoint(*geometry.Vector2d, *geometry.Vector4d) (res *matrix.DenseMatrix) {
	return
}

func (ak *AbstractKeyframe) UnProjectLinearize_WorldPoint(world_point, world_plane *geometry.Vector4d) (res *matrix.DenseMatrix) {
	t := geometry.Project(&world_point.Vector)
	return ak.UnProjectLinearize_ImagePoint(&geometry.Vector2d{*t}, world_plane)
}

// ============================ Video Keyframe ====================================
type Keyframe struct {
	AbstractKeyframe
	Camera ar_math.CameraModel
}

func NewKeyframe(img *image.Gray, scales int, cam ar_math.CameraModel) (kf *Keyframe) {
	kf = new(Keyframe)
	kf.AbstractKeyframe = *NewAbstractKeyframe(img, scales)
	kf.Camera = cam
	return
}

func (kf *Keyframe) Pose() *ar_math.SE3 {
	return &kf.pose
}

func (kf *Keyframe) Observations() []Measurement {
	return kf.observations
}

func (kf *Keyframe) Project(camera_point *geometry.Vector2d) *geometry.Vector2d {
	return kf.Camera.Project(camera_point)
}

func (kf *Keyframe) ProjectLinearize(camera_point *geometry.Vector2d) *matrix.DenseMatrix {
	image_point := kf.Camera.Project(camera_point)
	K := matrix.Zeros(3, 3)
	K.SetMatrix(0, 0, kf.Camera.GetProjectionDerivatives(camera_point))
	t, _ := K.GetMatrix(0, 0, 2, 2).TimesDense(&camera_point.DenseMatrix)
	t, _ = image_point.MinusDense(t)
	K.GetColVector(2).SetMatrix(0, 0, t)
	K.Set(2, 0, 0)
	K.Set(2, 1, 0)
	K.Set(2, 2, 1)
	return K
}

func (kf *Keyframe) Project_WorldPoint(world_point *geometry.Vector4d) *geometry.Vector2d {
	t := kf.pose.TimesDense(&world_point.DenseMatrix)
	v := geometry.Vector{*t.GetMatrix(0, 0, 3, 1)}
	v1 := kf.Project(&geometry.Vector2d{*geometry.Project(&v)})
	return v1
}

func (kf *Keyframe) ProjectLinearize_WorldPoint(world_point *geometry.Vector4d) *matrix.DenseMatrix {
	t := kf.pose.TimesDense(&world_point.DenseMatrix)
	v := geometry.Vector{*t.GetMatrix(0, 0, 3, 1)}
	camera_point := geometry.Vector2d{*geometry.Project(&v)}
	image_point := kf.Camera.Project(&camera_point)

	K := matrix.Zeros(3, 4)
	K.SetMatrix(0, 0, kf.Camera.GetProjectionDerivatives(&camera_point))
	t, _ = K.GetMatrix(0, 0, 2, 2).TimesDense(&camera_point.DenseMatrix)
	t, _ = image_point.MinusDense(t)
	K.GetColVector(2).SetMatrix(0, 0, t)
	K.Set(2, 0, 0)
	K.Set(2, 1, 0)
	K.Set(2, 2, 1)
	t, _ = K.TimesDense(kf.pose.GetMatrix())
	return t
}

func (kf *Keyframe) UnProject(img_point *geometry.Vector2d) *geometry.Vector2d {
	return kf.Camera.UnProject(img_point)
}

func (kf *Keyframe) UnProjectLinearize(image_point *geometry.Vector2d) *matrix.DenseMatrix {
	camera_point := kf.Camera.UnProject(image_point)
	K := matrix.Zeros(3, 3)

	K.SetMatrix(0, 0, kf.Camera.GetProjectionDerivatives(camera_point))
	t, _ := K.GetMatrix(0, 0, 2, 2).TimesDense(&camera_point.DenseMatrix)
	t, _ = image_point.MinusDense(t)
	K.GetColVector(2).SetMatrix(0, 0, t)
	K.Set(2, 0, 0)
	K.Set(2, 1, 0)
	K.Set(2, 2, 1)
	t, _ = K.Inverse()
	return t
}

func (kf *Keyframe) UnProjectToImgageplane(image_point *geometry.Vector2d) *geometry.Vector4d {
	camera_point := geometry.UnProject(&image_point.Vector)
	return geometry.MakeVector4d(camera_point.Get(0), camera_point.Get(1), 1, 1)
}

func (kf *Keyframe) UnProjectLinearize_ImagePoint(image_point *geometry.Vector2d,
	world_plane *geometry.Vector4d) *matrix.DenseMatrix {
	camera_plane, _ := world_plane.Transpose().TimesDense(kf.pose.Inverse().GetMatrix())
	camera_point := kf.Camera.UnProject(image_point)
	K := matrix.Zeros(3, 3)
	K.SetMatrix(0, 0, kf.Camera.GetProjectionDerivatives(camera_point))
	t, _ := K.GetMatrix(0, 0, 2, 2).TimesDense(&camera_point.DenseMatrix)
	t, _ = image_point.MinusDense(t)
	K.GetColVector(2).SetMatrix(0, 0, t)
	K.Set(2, 0, 0)
	K.Set(2, 1, 0)
	K.Set(2, 2, 1)

	v := geometry.Vector4d{geometry.Vector{*camera_plane}}
	intersection := intersect_perspective_matrix(&v)
	inv_K_T, _ := K.Transpose().Inverse()
	inv_K_T, _ = inv_K_T.TimesDense(intersection.Transpose())
	t = kf.pose.Inverse().TimesDense(inv_K_T.Transpose())
	return t
}

// ========================== OrthoKeyframe =======================================
type OrthoKeyframe struct {
	AbstractKeyframe
	projection matrix.DenseMatrix
	K_inv      matrix.DenseMatrix
}

func NewOrthoKeyframe(img *image.Gray, scales int, K *matrix.DenseMatrix) *OrthoKeyframe {
	okf := new(OrthoKeyframe)
	okf.AbstractKeyframe = *NewAbstractKeyframe(img, scales)
	okf.projection.SetMatrix(0, 0, geometry.MakeVector4d(1, 0, 0, 0).Transpose())
	okf.projection.SetMatrix(1, 0, geometry.MakeVector4d(0, 1, 0, 0).Transpose())
	okf.projection.SetMatrix(2, 0, geometry.MakeVector4d(0, 0, 0, 1).Transpose())

	t, _ := K.TimesDense(&okf.projection)
	okf.projection = *t
	t, _ = K.Inverse()
	okf.K_inv = *t
	return okf
}

func (okf *OrthoKeyframe) Pose() *ar_math.SE3 {
	return &okf.pose
}

func (okf *OrthoKeyframe) Observations() []Measurement {
	return okf.observations
}

func (okf *OrthoKeyframe) Project(camera_point *geometry.Vector2d) *geometry.Vector2d {
	v4 := geometry.MakeVector4d(camera_point.Get(0), camera_point.Get(1), 0, 1)
	t, _ := okf.projection.TimesDense(&v4.DenseMatrix)
	return &geometry.Vector2d{*geometry.Project(&geometry.Vector{*t})}
}

func (okf *OrthoKeyframe) ProjectLinearize(camera_point *geometry.Vector2d) *matrix.DenseMatrix {

	K := matrix.Zeros(3, 3)
	K.SetMatrix(0, 0, okf.projection.GetMatrix(0, 0, 3, 2))
	K.SetMatrix(0, 2, okf.projection.GetMatrix(0, 3, 3, 1))
	return K
}

func (okf *OrthoKeyframe) Project_WorldPoint(world_point *geometry.Vector4d) *geometry.Vector2d {
	t, _ := okf.projection.TimesDense(okf.pose.GetMatrix())
	t, _ = t.TimesDense(&world_point.DenseMatrix)
	v := geometry.Project(&geometry.Vector{*t})
	return &geometry.Vector2d{*v}
}

func (okf *OrthoKeyframe) ProjectLinearize_WorldPoint(world_point *geometry.Vector4d) *matrix.DenseMatrix {
	t, _ := okf.projection.TimesDense(okf.pose.GetMatrix())
	return t
}

func (okf *OrthoKeyframe) UnProject(image_point *geometry.Vector2d) *geometry.Vector2d {
	t, _ := okf.K_inv.TimesDense(&geometry.UnProject(&image_point.Vector).DenseMatrix)
	t1 := geometry.Vector{*t}
	return &geometry.Vector2d{*geometry.Project(&t1)}
}

func (okf *OrthoKeyframe) UnProjectLinearize(image_point *geometry.Vector2d) *matrix.DenseMatrix {
	return &okf.K_inv
}

func (okf *OrthoKeyframe) UnProjectToImgageplane(image_point *geometry.Vector2d) *geometry.Vector4d {
	camera_point := geometry.UnProject(&image_point.Vector)
	return geometry.MakeVector4d(camera_point.Get(0), camera_point.Get(1), 0, 1)
}

func (okf *OrthoKeyframe) UnProjectLinearize_ImagePoint(image_point *geometry.Vector2d,
	world_plane *geometry.Vector4d) *matrix.DenseMatrix {
	t, _ := world_plane.Transpose().TimesDense(okf.pose.Inverse().GetMatrix())
	intersection := intersect_ortho_matrix(&geometry.Vector4d{geometry.Vector{*t}})
	t = okf.pose.Inverse().TimesDense(intersection)
	t, _ = t.TimesDense(&okf.K_inv)
	return t
}

// PointFeature represent 3D point feature of sparse 3D model reconstructed.
type PointFeature struct {
	frame                 KeyframeInterface   // which image to use for appearance calculations
	location              geometry.Vector2d   // location in the image
	keypoint              feature2d.KeyPoint  // keypoint information
	measurement_keyframes []KeyframeInterface // keyframes that see this point
	visible               uint                // statistics for checking how often it was seen
	tracked               uint                // statistics for checking how often it was tracked

	location_camera geometry.Vector2d // location in the camera frame
	center          geometry.Vector4d // 3D position of the point in world coordinates
	plane           geometry.Vector4d // plane equation of the point in world coordinates

	inlier  uint
	outlier uint
	bad     bool // bad pointfeature have high ratio of outliers
	trashed bool // this pointfeature should be trashed
}

func (pf *PointFeature) init() {
	pf.visible = 0
	pf.tracked = 0
	pf.inlier = 0
	pf.outlier = 0
	pf.bad = false
	pf.trashed = false
}

func (pf *PointFeature) updateCoordinates() {
	t, _ := pf.frame.UnProjectLinearize_ImagePoint(&pf.location, &pf.plane).TimesDense(&geometry.UnProject(&pf.location.Vector).DenseMatrix)
	if t.Get(3, 0) < 0 {
		t = matrix.Scaled(t, -1)
	}
	// negative w don't play well for sign comparisons (plane equations etc.),
	// we are effectivily using a signed projective space
	//assert(center[3] >= 0);
	pf.location_camera = *pf.frame.UnProject(&pf.location)
}

func NewPointFeature(kf KeyframeInterface, location *geometry.Vector2d, support_plane *geometry.Vector4d) *PointFeature {
	pf := new(PointFeature)
	pf.frame = kf
	pf.location = *location
	pf.plane = *support_plane
	pf.init()
	pf.updateCoordinates()
	return pf
}

func (pf *PointFeature) GetClosetObservation(kf KeyframeInterface, observations []Measurement) *Measurement {
	d_mindist := 1.e+9
	var meas_closest *Measurement
	v3_current := kf.Pose().Inverse().GetTranslation()
	for i := 0; i < len(observations); i++ {
		frame := observations[i].frame
		t, _ := v3_current.MinusDense(&frame.Pose().Inverse().GetTranslation().DenseMatrix)
		v := geometry.Vector{*t}
		dist := v.Norm()
		if d_mindist > dist {
			d_mindist = dist
			meas_closest = &observations[i]
		}
	}
	return meas_closest
}

func (pf *PointFeature) CalculateMapping(view KeyframeInterface) (*geometry.Vector2d, *matrix.DenseMatrix) {
	world4view := view.UnProjectLinearize_WorldPoint(&pf.center, &pf.plane)
	frame4world := pf.frame.ProjectLinearize_WorldPoint(&pf.center)
	frame4view, _ := frame4world.TimesDense(world4view)

	if frame4view.Det() < -1 {
		frame4view = matrix.Scaled(frame4view, -1)
	}
	c := view.Project_WorldPoint(&pf.center)
	return c, linearize_homography(frame4view, c)
}

func (pf *PointFeature) CalculateMapping_Closet(view, kf_closest KeyframeInterface) (*geometry.Vector2d, *matrix.DenseMatrix) {
	world4view := view.UnProjectLinearize_WorldPoint(&pf.center, &pf.plane)
	frame4world := kf_closest.ProjectLinearize_WorldPoint(&pf.center)
	frame4view, _ := frame4world.TimesDense(world4view)
	if frame4view.Det() < -1 {
		frame4view = matrix.Scaled(frame4view, -1)
	}
	c := view.Project_WorldPoint(&pf.center)
	return c, linearize_homography(frame4view, c)
}

// Measurement represent an observation of PointFeature in specific Keyframe.
type Measurement struct {
	location geometry.Vector2d // found location of a Feature on a Frame (on Image plane)
	feature  *PointFeature     // pointer to the observed feature
	frame    KeyframeInterface // pointer to the observed frame
}

func NewMeasurement(f *PointFeature, kf KeyframeInterface,
	location *geometry.Vector2d) *Measurement {
	m := new(Measurement)
	m.feature = f
	m.frame = kf
	m.location = *location
	return m
}
