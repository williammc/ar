// Copyright 2014 The Authors. All rights reserved.

package track

import (
	ar_math "bitbucket.org/williammccohen/ar/math"
	"bitbucket.org/williammccohen/cv/feature2d"
	"bitbucket.org/williammccohen/cv/geometry"
	matrix "github.com/skelterjohn/go.matrix"
	"image"
)

// Represent a Keyframe based model.
type Model struct {
	keyframes []KeyframeInterface
	features  []*PointFeature
}

// Get list of indices to visible features.
func (m *Model) FindVisibleFeatures(vf *ViewFrustum) (result []uint) {
	for i := 0; i < len(m.features); i++ {
		if !m.features[i].trashed && vf.Visible(&m.features[i].center) {
			result = append(result, uint(i))
		}
	}
	return
}

func CreateVideoKeyframe(features []*PointFeature, frame *image.Gray,
	local_camera *ar_math.CameraModel,
	distance float64, pose *ar_math.SE3, scales int,
	detector feature2d.FeatureDetector, toonear_offset float64) *Keyframe {

	keyframe := NewKeyframe(frame, scales, *local_camera)
	keyframe.pose = *pose
	support_plane := geometry.MakeVector4d(0, 0, 0, 1)
	if distance >= 0 {
		support_plane = geometry.MakeVector4d(0, 0, -1, distance)
	}
	keypoints := detector.Detect(frame)

	// elimiate too-close eachother features
	var desired_keypoints []feature2d.KeyPoint
	for i := 0; i < len(keypoints); i++ {
		tooclose := false
		// too near to observed features
		for j := 0; j < len(keyframe.Observations()) && !tooclose; j++ {

			t, _ := keypoints[i].Location.MinusDense(&keyframe.observations[j].location.DenseMatrix)
			v := geometry.Vector{*t}
			if v.Norm() < toonear_offset {
				tooclose = true
			}
		}
		// too near to each others
		for j := 0; j < len(desired_keypoints) && tooclose; j++ {
			t, _ := keypoints[i].Location.MinusDense(&desired_keypoints[j].Location.DenseMatrix)
			v := geometry.Vector{*t}
			if v.Norm() < toonear_offset {
				tooclose = true
			}
		}
		if !tooclose {
			desired_keypoints = append(desired_keypoints, keypoints[i])
		}
	}

	keypoints = desired_keypoints
	for i := 0; i < len(keypoints); i++ {
		pf := NewPointFeature(keyframe, &keypoints[i].Location, support_plane)
		pf.keypoint = keypoints[i]
		features = append(features, pf)
		meas := NewMeasurement(pf, keyframe, &keypoints[i].Location)
		keyframe.observations = append(keyframe.observations, *meas)
	}
	return keyframe

}

// add a VIDEOKEYFRAME and reuse the main camera
// feature points are on a plane parallel to the image plane at a fixed distance
func AddFrame(model *Model, frame *image.Gray, local_camera *ar_math.CameraModel,
	distance float64, pose *ar_math.SE3, scales int, detector feature2d.FeatureDetector,
	toonear_offset float64) {
	var features []*PointFeature
	keyframe := CreateVideoKeyframe(features, frame, local_camera, distance,
		pose, scales, detector, toonear_offset)
	model.keyframes = append(model.keyframes, keyframe)
	model.features = append(model.features, features[:]...)
}

// create an ORTHOGRAPHIC keyframe and set linear  camera from dimensions
// feature points are on a plane with distance 0
func CreateOrthoKeyframe(features []*PointFeature, frame *image.Gray,
	dimensions *geometry.Vector2d, pose *ar_math.SE3, scales int,
	detector feature2d.FeatureDetector) *OrthoKeyframe {
	// build an Orthographic camera
	K := matrix.Eye(3)
	K.Set(0, 0, float64(frame.Bounds().Dx())/dimensions.Get(0))
	K.Set(1, 1, float64(frame.Bounds().Dy())/dimensions.Get(1))
	K.Set(0, 2, float64(frame.Bounds().Dx())*0.5)
	K.Set(1, 2, float64(frame.Bounds().Dy())*0.5)
	keyframe := NewOrthoKeyframe(frame, scales, K)
	keyframe.pose = *pose
	support_plane := geometry.MakeVector4d(0.0, 0, 1, 0)
	keypoints := detector.Detect(frame)
	for i := 0; i < len(keypoints); i++ {
		pf := NewPointFeature(keyframe, &keypoints[i].Location, support_plane)
		pf.keypoint = keypoints[i]
		features = append(features, pf)
		meas := NewMeasurement(pf, keyframe, &keypoints[i].Location)
		keyframe.observations = append(keyframe.observations, *meas)

	}
	return keyframe
}

// add an ORTHOGRAPHIC keyframe and set linear  camera from dimensions
// feature points are on a plane with distance 0
func AddOrthoKeyframe(model *Model, frame *image.Gray, dimensions *geometry.Vector2d,
	pose *ar_math.SE3, scales int, detector feature2d.FeatureDetector) {
	var features []*PointFeature
	keyframe := CreateOrthoKeyframe(features, frame, dimensions,
		pose, scales, detector)
	model.keyframes = append(model.keyframes, keyframe)
	model.features = append(model.features, features[:]...)
}
