// Copyright 2014 The Authors. All rights reserved.

package track

import (
	ar_math "bitbucket.org/williammccohen/ar/math"
	"bitbucket.org/williammccohen/cv/geometry"
	matrix "github.com/skelterjohn/go.matrix"
)

type OptmizationObs struct {
	world_point geometry.Vector4d
	cam_point   geometry.Vector2d
}

func OptimisePose(input []OptmizationObs, pose ar_math.SE3) (ar_math.SE3, float64) {

	sum_squared_error := 0.0
	delta := geometry.MakeVector(6)

	for iteration := 0; delta.Norm() > 1.e-9 && iteration < 10; iteration++ {
		JTJ := matrix.Zeros(6, 6)
		JTE := matrix.Zeros(6, 1)
		J_pose := matrix.Zeros(2, 6)

		// calculate sigma
		squared_errs := make([]float64, 0, len(input))
		for i := 0; i < len(input); i++ {
			t := pose.TimesDense(&input[i].world_point.DenseMatrix)
			error, _ := input[i].cam_point.MinusDense(&geometry.Project(&geometry.Vector{*t}).DenseMatrix)
			verror := geometry.Vector{*error}
			squared_errs = append(squared_errs, verror.SquaredNorm())
		}
		est := new(Tukey)
		est.ComputeSigmaSquared(squared_errs)
		sum_squared_error = 0.0

		// do optimisation
		for i := 0; i < len(input); i++ {
			error, _ := input[i].cam_point.MinusDense(
				&transform_and_project(&pose, &input[i].world_point, J_pose).DenseMatrix)
			te := &geometry.Vector{*error}
			w := est.Weight(te.SquaredNorm())
			t, _ := matrix.Scaled(J_pose.Transpose(), w).TimesDense(J_pose)
			JTJ.AddDense(t)
			t, _ = matrix.Scaled(J_pose.Transpose(), w).TimesDense(error)
			JTE.AddDense(t)
		}
		tdelta, _ := JTJ.Inverse()
		tdelta, _ = tdelta.TimesDense(JTE)
		delta = &geometry.Vector{*tdelta}
		pose = *ar_math.SE3Exp(delta).Times(&pose)
	}
	return pose, sum_squared_error
}

func transform_and_project(pose *ar_math.SE3, x *geometry.Vector4d,
	J_pose *matrix.DenseMatrix) *geometry.Vector2d {
	in_frame := pose.TimesDense(&x.DenseMatrix)
	z_inv := 1.0 / in_frame.Get(2, 0)
	x_z_inv := in_frame.Get(0, 0) * z_inv
	y_z_inv := in_frame.Get(1, 0) * z_inv
	cross := x_z_inv * y_z_inv
	J_pose.Set(0, 0, z_inv*in_frame.Get(3, 0))
	J_pose.Set(1, 1, z_inv*in_frame.Get(3, 0))

	J_pose.Set(0, 1, 0.0)
	J_pose.Set(1, 0, 0.0)

	J_pose.Set(0, 2, -x_z_inv*z_inv*in_frame.Get(3, 0))
	J_pose.Set(1, 2, -y_z_inv*z_inv*in_frame.Get(3, 0))

	J_pose.Set(0, 3, -cross)
	J_pose.Set(0, 4, 1.0+x_z_inv*x_z_inv)
	J_pose.Set(0, 5, -y_z_inv)

	J_pose.Set(1, 3, -1.0-y_z_inv*y_z_inv)
	J_pose.Set(1, 4, cross)
	J_pose.Set(1, 5, x_z_inv)

	return geometry.MakeVector2d(x_z_inv, y_z_inv)
}
