// Copyright 2014 The Authors. All rights reserved.

package track

import (
	//"fmt"
	"bitbucket.org/williammccohen/ar/geometry"
	"bitbucket.org/williammccohen/ar/util"
	"github.com/nfnt/resize"
	goar_math "goar/math"
	"image"
	"image/color"
	"math/rand"
	"runtime"
	"testing"
)

var (
	img_source = image.NewGray(image.Rect(0, 0, 640, 480))
	img_target *image.Gray
	ratio      float64
	pf         *PatchFinder
	patch_size int = 8
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	// randomly assigning pixels
	for y := 0; y < img_source.Bounds().Dy(); y++ {
		for x := 0; x < img_source.Bounds().Dx(); x++ {
			img_source.Set(x, y, color.Gray{uint8(rand.Intn(255))})
		}
	}

	ratio = 0.75
	new_width := uint(float64(img_source.Bounds().Dx()) * ratio)
	new_height := uint(float64(img_source.Bounds().Dy()) * ratio)
	img_target = resize.Resize(new_width, new_height, img_source, resize.NearestNeighbor).(*image.Gray)

	pf = NewPatchFinder(patch_size, 5, true)
	pf.SetSearchImage(img_target)
}

// Length of normalized vector has to be one.
func TestPatchFinder(t *testing.T) {

	sum_norm := 0.0
	for y := 50; y < 60; y++ {
		for x := 50; x < 70; x++ {
			v2 := geometry.MakeVector2d(float64(x), float64(y))
			v2_realtarget := geometry.MakeVector2d(float64(x)*ratio, float64(y)*ratio)
			v2_guess := geometry.MakeVector2d(float64(x)*ratio-2, float64(y)*ratio-3)

			t1 := int(v2.Get(0) - float64(patch_size)/2)
			t2 := int(v2.Get(1) - float64(patch_size)/2)
			m_patch := img_source.SubImage(image.Rect(t1, t2, patch_size, patch_size)).(*image.Gray)

			loc, _ := pf.Search(m_patch, v2_guess)
			v, _ := v2_realtarget.MinusDense(&loc.DenseMatrix)
			sum_norm += goar_math.Norm(v)
		}
	}

	if !util.Check_CloseEqual(sum_norm, 0, 1.e-3) {
		t.Fail()
	}
}

// Benchmark for a transformation of 640x480 grayscale image
func BenchmarkPatchFinder_On640x480(b *testing.B) {
	b.StopTimer()
	v2_guess := geometry.MakeVector2d(50, 52)
	t1 := int(v2_guess.Get(0) - float64(patch_size)/2)
	t2 := int(v2_guess.Get(1) - float64(patch_size)/2)
	m_patch := img_source.SubImage(image.Rect(t1, t2, patch_size, patch_size)).(*image.Gray)
	b.StartTimer()
	_, _ = pf.Search(m_patch, v2_guess)
}
