// Copyright 2014 The Authors. All rights reserved.

package track

import (
	ar_math "bitbucket.org/williammccohen/ar/math"
	"bitbucket.org/williammccohen/cv/geometry"
	matrix "github.com/skelterjohn/go.matrix"
	"log"
)

type ViewFrustum struct {
	frustum matrix.DenseMatrix
}

func NewViewFrustum(cam ar_math.CameraModel, size []float64) *ViewFrustum {
	vf := new(ViewFrustum)
	ul := geometry.UnProject(&cam.UnProject(geometry.MakeVector2d(0.0, 0.0)).Vector)
	ll := geometry.UnProject(&cam.UnProject(geometry.MakeVector2d(0.0, size[1])).Vector)
	ur := geometry.UnProject(&cam.UnProject(geometry.MakeVector2d(size[0], 0.0)).Vector)
	lr := geometry.UnProject(&cam.UnProject(geometry.MakeVector2d(size[0], size[1])).Vector)

	vf.frustum = *matrix.Zeros(4, 4)
	vf.frustum.Set(0, 0, ll.Cross(ul).Normalize().Transpose().Get(0, 0)) // left plane
	vf.frustum.Set(1, 0, ul.Cross(ur).Normalize().Transpose().Get(0, 0)) // upper plane
	vf.frustum.Set(2, 0, ur.Cross(lr).Normalize().Transpose().Get(0, 0)) // right plane
	vf.frustum.Set(3, 0, lr.Cross(ll).Normalize().Transpose().Get(0, 0)) // lower plane
	t := geometry.MakeVector2d(size[0]*0.5, size[1]*0.5)
	center := geometry.UnProject(geometry.UnProject(&cam.UnProject(t).Vector))

	t1, _ := vf.frustum.TimesDense(&center.DenseMatrix)
	if t1.Get(0, 0) <= 0 ||
		t1.Get(1, 0) <= 0 ||
		t1.Get(2, 0) <= 0 ||
		t1.Get(3, 0) <= 0 {
		log.Fatal("ViewFrustum:: wrong frustum")
	}

	return vf
}

func (vf *ViewFrustum) Visible(p *geometry.Vector4d) bool {
	t, _ := vf.frustum.TimesDense(&p.DenseMatrix)
	return (t.Get(0, 0) > 0 && t.Get(1, 0) > 0 && t.Get(2, 0) > 0 && t.Get(3, 0) > 0)
}

func (vf *ViewFrustum) Transform(tr *ar_math.SE3) {
	t, _ := vf.frustum.TimesDense(tr.Inverse().GetMatrix())
	vf.frustum.SetMatrix(0, 0, t)
}
