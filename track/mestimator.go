// Copyright 2014 The Authors. All rights reserved.

package track

import (
	"math"
	"sort"
)

type Tukey struct {
	outliers      int
	sigma_squared float64
}

func (t *Tukey) ComputeSigmaSquared(values []float64) float64 {
	nvalues := len(values)
	if nvalues == 0 {
		return 0
	}
	sort.Float64s(values)
	median_squared := values[nvalues/2]
	if median_squared == 0 {
		median_squared = 1.e-9
	}
	sigma := 1.4826 * (1 + 5.0/(float64(nvalues)*2.0-6.0)) * math.Sqrt(median_squared)
	sigma = 4.6851 * sigma
	t.sigma_squared = sigma * sigma
	return t.sigma_squared

}

func (t *Tukey) Weight(error_squared float64) float64 {
	if error_squared > t.sigma_squared {
		t.outliers++
		return 0.0
	} else {
		w := 1.0 - error_squared/t.sigma_squared
		return w * w
	}
	return 0.0
}
