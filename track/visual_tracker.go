// Copyright 2014 The Authors. All rights reserved.

package track

import (
	ar_math "bitbucket.org/williammccohen/ar/math"
	"bitbucket.org/williammccohen/cv/geometry"
	matrix "github.com/skelterjohn/go.matrix"
	"image"
	"math"
)

const (
	GOOD = iota
	OK
	BAD
	FAILURE
)

type VisualTracker struct {
	model *Model // Keyframe based 3D Model

	video_camera ar_math.CameraModel
	pose         ar_math.SE3
	last_pose    ar_math.SE3

	// stats on last tracking
	last_level         int
	visible            uint
	tracked            uint
	reprojection_error float64
	ncc_threshold      float64
	view_threshold     float64
	outlier_threshold  float64

	scales       int
	start_level  int
	motion_model bool
	prerot       bool
	speed        float64
	debug_mode   bool
	numbers      []uint
	radius       []uint
}

func NewVisualTracker(cam_params []float64, size []int, scales int, start_level int) {
	vt := new(VisualTracker)
	vt.scales = scales
	vt.start_level = start_level
	vt.ncc_threshold = 0.7 * 0.7
	vt.motion_model = true
	vt.prerot = false
	vt.speed = 1.0
	vt.outlier_threshold = 0.9
	vt.debug_mode = false
	vt.video_camera = ar_math.MakePoliCamera(size[0], size[1])
	vt.video_camera.SetParameters(cam_params)
	vt.model = nil

	// numbers of features to search on each level
	//vt.numbers = append(vt.numbers, []uint{200, 100, 20, 20} ...)

	vt.numbers = append(vt.numbers, []uint{400, 300, 100, 100}...)
	// size of search radius at each level
	vt.radius = append(vt.radius, []uint{2, 2, 4, 4}...)
}

func (vt *VisualTracker) SetModel(m *Model) {
	vt.model = m
}

func (vt *VisualTracker) ProcessFrame(img *image.Gray) {
	search := NewKeyframe(img, vt.scales, vt.video_camera)
	vt.TrackFrame(search)
}

// returns the result of the  last tracking operation.
// Ok and NEW_FRAME mean that tracking was good
// BAD tracking was not so good but still possible
// FAILURE is tracking failed so no new pose
func (vt *VisualTracker) GetTrackResult() int {
	ratio := float64(vt.tracked) / math.Max(float64(vt.visible), 1.0)
	if ratio < 0.1 || vt.tracked < 10 { // ||last_level != 0
		return FAILURE
	}
	if ratio < 0.3 {
		return BAD
	}
	if vt.reprojection_error > 5.e-2 {
		return OK
	}
	return GOOD
}

// get reprojection error on image plane (pixel coordinate)
func (vt *VisualTracker) GetReprojectionError() float64 {

	v6_cam := vt.video_camera.Parameters()
	return vt.reprojection_error * math.Max(v6_cam[0], v6_cam[1])
}

// set the current orientation  of the camera
// @param p new orientation
func (vt *VisualTracker) SetPose(p ar_math.SE3) {
	vt.pose = p
	vt.last_pose = p
}

// returns the current camera orientation
func (vt *VisualTracker) GetPose() *ar_math.SE3 {
	return &vt.pose
}

// returns the camera calibration used by the tracker
func (vt *VisualTracker) get_camera() ar_math.CameraModel {
	return vt.video_camera
}

func (vt *VisualTracker) SetDebugMode(onoff bool) { vt.debug_mode = onoff }
func (vt *VisualTracker) DebugMode() bool         { return vt.debug_mode }

func (vt *VisualTracker) SetWithMotionmodel(onoff bool) { vt.motion_model = onoff }
func (vt *VisualTracker) WithMotionmodel() bool         { return vt.motion_model }

func (vt *VisualTracker) SetWithPrerot(onoff bool) { vt.prerot = onoff }
func (vt *VisualTracker) WithPrerot() bool         { return vt.prerot }

func (vt *VisualTracker) SetStartLevel(sl int) { vt.start_level = sl }
func (vt *VisualTracker) StartLevel() int      { return vt.start_level }

func (vt *VisualTracker) SetSpeed(s float64) { vt.speed = s }
func (vt *VisualTracker) Speed() float64     { return vt.speed }

func (vt *VisualTracker) LastVisible() uint { return vt.visible }
func (vt *VisualTracker) LastTracked() uint { return vt.tracked }

// Track frame
func (vt *VisualTracker) TrackFrame(search *Keyframe) {

	if vt.model == nil {
		return
	}

	t := matrix.Scaled(&vt.pose.Times(vt.last_pose.Inverse()).Ln().DenseMatrix, 0.5)

	motion_delta := ar_math.SE3Exp(&geometry.Vector{*t})
	vt.last_pose = vt.pose
	if vt.motion_model {
		vt.pose = *motion_delta.Times(&vt.pose)
	}

	oldpose := vt.pose
	vt.last_level = 3
	var visible_indices [][]uint
	var corrs [][]geometry.Vector2d

	for level := vt.start_level; level > -1; level-- {
		temp_indices, temp_corrs := vt.TrackLevel(search, level, uint(float64(vt.numbers[level])*vt.speed), vt.radius[level])
		if !vt.OptimisePose(temp_indices, temp_corrs) {
			break
		}
		vt.last_level = level
		corrs = append(corrs, temp_corrs)
		visible_indices = append(visible_indices, temp_indices)
	}

	if vt.GetTrackResult() == FAILURE {
		vt.pose = oldpose
	} else { // possible that this new keyframe is useful
		// map feature index to location
		var m_featureid_location map[uint]geometry.Vector2d
		// get observations from fine to coarse
		for i := 0; i <= vt.start_level; i++ {
			for j := 0; j < len(corrs[i]); j++ {
				if _, found := m_featureid_location[visible_indices[i][j]]; !found {
					m_featureid_location[visible_indices[i][j]] = corrs[i][j]
				}
			}
		}
		// add observations to new keyframe
		for id, v := range m_featureid_location {
			meas := NewMeasurement(vt.model.features[id], search, vt.video_camera.Project(&v))
			search.observations = append(search.observations, *meas)
		}
	}
}

// Track one level
func (vt *VisualTracker) TrackLevel(input *Keyframe, level int,
	numbers, radius uint) ([]uint, []geometry.Vector2d) {
	finder := NewPatchFinder(8, int(radius), true) // patch_size:8, subpixel:true
	finder.SetSearchImage(input.scale_images[level])
	patch := image.NewGray(image.Rect(0, 0, 8, 8))

	width, height := vt.video_camera.GetResolution()

	vf := NewViewFrustum(vt.video_camera, []float64{float64(width), float64(height)})
	vf.Transform(vt.pose.Inverse())

	access := vt.model.FindVisibleFeatures(vf)
	//random_shuffle(access.begin(), access.end());
	input.pose = vt.pose
	var final_access []uint
	var final_obs []geometry.Vector2d
	for i := 0; i < len(access) && vt.visible < numbers; i++ {
		f := vt.model.features[access[i]]
		f.visible++
		frame := f.frame
		p, m := f.CalculateMapping(input)
		vp := scale_by_levels(p, -level)
		if finder.CheckLocation(vp) {

			// work out right scale here
			mapping := matrix.Scaled(m, scale_from_level(level))
			tb, _ := frame.Sample(&f.location, mapping, patch)
			if !tb {
				continue
			}
			loc, score := finder.Search(patch, vp)
			if score > vt.ncc_threshold {
				final_access = append(final_access, access[i])
				final_obs = append(final_obs, *vt.video_camera.UnProject(scale_by_levels(loc, level)))
				f.tracked++
			}

			vt.visible++

		}
	}
	input.visiblefeatures = access
	return final_access, final_obs
}

func (vt *VisualTracker) OptimisePose(visible_indices []uint, obs []geometry.Vector2d) bool {

	if len(obs) < 5 {
		return false
	}

	input := make([]OptmizationObs, len(obs), len(obs))
	for i := 0; i < len(obs); i++ {
		input[i] = OptmizationObs{vt.model.features[visible_indices[i]].center, obs[i]}
	}

	vt.pose, vt.reprojection_error = OptimisePose(input, vt.pose)
	return true
}
