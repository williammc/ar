// Copyright 2014 The Authors. All rights reserved.

package util

import (
	"bitbucket.org/williammccohen/cv/geometry"
	"bitbucket.org/williammccohen/cv/imgproc"
	"image"
)

func InImageWithBorder(rect image.Rectangle, location image.Point, border int) bool {
	return location.X >= border && location.Y >= border &&
		location.X < rect.Dx()-border && location.Y < rect.Dy()-border
}

// @param I__1__1 Pixel $(-1, -1)$ relative to the centre (a)
// @param I__1_0  Pixel $(-1,  0)$ relative to the centre (b)
// @param I__1_1  Pixel $(-1,  1)$ relative to the centre (c)
// @param I_0__1  Pixel $( 0, -1)$ relative to the centre (d)
// @param I_0_0   Pixel $( 0,  0)$ relative to the centre (e)
// @param I_0_1   Pixel $( 0,  1)$ relative to the centre (f)
// @param I_1__1  Pixel $( 1, -1)$ relative to the centre (g)
// @param I_1_0   Pixel $( 1,  0)$ relative to the centre (h)
// @param I_1_1   Pixel $( 1,  1)$ relative to the centre (i)
// @return pair containing Location of the local extrema and the value
// @ingroup gVision
func internalInterpolateExtremumValue(
	I__1__1,
	I__1_0,
	I__1_1,
	I_0__1,
	I_0_0,
	I_0_1,
	I_1__1,
	I_1_0,
	I_1_1 float64) (*geometry.Vector2d, float64) {
	// Compute the gradient values
	gx := 0.5 * (I_1_0 - I__1_0)
	gy := 0.5 * (I_0_1 - I_0__1)

	// Compute the Hessian values
	gxx := I_1_0 - 2*I_0_0 + I__1_0
	gyy := I_0_1 - 2*I_0_0 + I_0__1
	gxy := 0.25 * (I_1_1 + I__1__1 - I_1__1 - I__1_1)

	// Compute -inv(H) * grad
	Dinv := 1. / (gxx*gyy - gxy*gxy)

	v := []float64{0, 0}
	v[0] = -Dinv * (gyy*gx - gxy*gy)
	v[1] = -Dinv * (-gxy*gx + gxx*gy)
	value := I_0_0 + (gx+0.5*gxx*v[0]+gxy*v[1])*
		v[0] + (gy+0.5*gyy*v[1])*v[1]
	return geometry.MakeVector2d(v[0], v[1]), value
}

// Interpolate a 2D local maximum, by fitting a quadratic.
// @param i Image in which to interpolate extremum
// @param p Point at which to interpolate extremum
// @return pair containing Location of local extremum
//   in image coordinates and the value of the extemum
// @ingroup gVision
func InterpolateExtremumValue(I *imgproc.Float64Image, location *geometry.Vector2d) (*geometry.Vector2d, float64) {
	p := []int{int(location.Get(0)), int(location.Get(1))}
	// Extract and label 9 particular points
	I__1__1 := I.At(p[0]-1, p[1]-1)
	I__1_0 := I.At(p[0]-1, p[1])
	I__1_1 := I.At(p[0]-1, p[1]+1)
	I_0__1 := I.At(p[0], p[1]-1)
	I_0_0 := I.At(p[0], p[1])
	I_0_1 := I.At(p[0], p[1]+1)
	I_1__1 := I.At(p[0]+1, p[1]-1)
	I_1_0 := I.At(p[0]+1, p[1])
	I_1_1 := I.At(p[0]+1, p[1]+1)

	loc, value :=
		internalInterpolateExtremumValue(I__1__1, I__1_0, I__1_1,
			I_0__1, I_0_0, I_0_1,
			I_1__1, I_1_0, I_1_1)

	_ = loc.AddDense(&geometry.MakeVector2d(float64(p[0]), float64(p[1])).DenseMatrix)
	return loc, value
}
