// Copyright 2014 The Authors. All rights reserved.

package util

import (
	"math"
)

func Check_CloseEqual(a, b float64, args ...float64) bool {
	gap := float64(1.e-9)
	if len(args) > 0 {
		gap = args[0]
	}
	return (math.Abs(a-b) < gap)
}
