// Copyright 2014 The Authors. All rights reserved.

package ui

import (
	ar_math "bitbucket.org/williammccohen/ar/math"
	"bitbucket.org/williammccohen/cv/geometry"
	"github.com/go-gl/gl"
	matrix "github.com/skelterjohn/go.matrix"
	"log"
	"math"
)

// Render modes
const (
	UNDISTORT = iota
	DISTORT
	ORIGINAL
)

// Drawable object interface.
type AbstractDrawable interface {
	Draw()
	Draw2D()
}

// sampling step used for texture mapping (coordinate generation)
const sample_steps = 48

// AR Render
type ARRender struct {
	rendermode        int
	video_camera      ar_math.CameraModel
	drawables         []AbstractDrawable
	videoframe_pixels []uint8
	pose              ar_math.SE3

	released bool

	// distorted coords in Vision coordinates (start upper-left)
	dist_texcoords []float64
	// undistorted vertexs in OpenGL coordinates (start lower-left)
	undist_vertexs []float64

	// undistorted coords in OpenGL coordinates
	undist_texcoords []float64
	// distorted vertex in OpenGL coordinates
	dist_vertexs []float64

	// texture stuffs
	frametexture        gl.Texture
	frametexture_width  int
	frametexture_height int

	// frame-buffer-object for AR Rendering
	framebuffer             gl.Framebuffer
	framebuffer_texture     gl.Texture
	framebuffer_depthbuffer gl.Renderbuffer
	framebuffer_width       int
	framebuffer_height      int
	texture_initialized     bool

	// window stuffs
	currentviewport_width  int
	currentviewport_height int
}

func NewARRender() *ARRender {
	ar := new(ARRender)
	ar.released = false
	ar.texture_initialized = false
	return ar
}

func (ar *ARRender) Init() {
	gl.Enable(gl.LIGHT0)
	gl.Enable(gl.LIGHTING)
	gl.Enable(gl.DEPTH_TEST)
	gl.Enable(gl.COLOR_MATERIAL)
}

func (ar *ARRender) Configure(camera ar_math.CameraModel) {
	ar.video_camera = camera
	ar.rendermode = DISTORT
	ar.frametexture_width, ar.frametexture_height = ar.video_camera.GetResolution()

	// work out FrameBuffer size to cover undistorted video frame
	tl := ar.video_camera.ProjectLinear(ar.video_camera.UnProject(geometry.MakeVector2d(0, 0)))
	br := ar.video_camera.ProjectLinear(ar.video_camera.UnProject(
		geometry.MakeVector2d(float64(ar.frametexture_width), float64(ar.frametexture_height))))
	t, _ := br.MinusDense(&geometry.MakeVector2d(float64(ar.frametexture_width), float64(ar.frametexture_height)).DenseMatrix)
	offset := geometry.Vector{*t}
	if offset.Norm() < tl.Norm() {
		offset.DenseMatrix = *matrix.Scaled(&tl.DenseMatrix, -1.0)
	}

	max_ratio := math.Max((float64(ar.frametexture_width)+offset.Get(0))/float64(ar.frametexture_width),
		(float64(ar.frametexture_height)+offset.Get(1))/float64(ar.frametexture_height))
	max_ratio *= 1.2 // larger to surely cover undistorted video frame texture

	ar.framebuffer_width = int(float64(ar.frametexture_width) * max_ratio)
	ar.framebuffer_height = int(float64(ar.frametexture_height) * max_ratio)
	ar.GenerateTextureMappingCoordinates()
	ar.texture_initialized = false
}

func (ar *ARRender) Render() {

	if !ar.texture_initialized {
		ar.GenerateFrameBuffers()
	}
	ar.UploadVideoFrame()
	if ar.rendermode == ORIGINAL {
		return
	}

	ar.set_frustum(0.001, 1.e+6) // render knows what the frustum looks like :)
	gl.Enable(gl.DEPTH_TEST)
	gl.MatrixMode(gl.MODELVIEW)
	gl.PushMatrix()
	gl.LoadIdentity()
	gl.Rotated(180, 1, 0, 0) // rotate around X-axis 180degree (vision to OpenGL)
	m4 := ar.pose.GetMatrix()

	glMultMatrix(m4)
	for i := 0; i < len(ar.drawables); i++ {
		ar.drawables[i].Draw()
	}

	gl.MatrixMode(gl.MODELVIEW)
	gl.PopMatrix()
	if ar.rendermode == ORIGINAL {
		return
	}
	// map FBO Texture to default FBO
	ar.MapFBO2SystemFBO()

	for i := 0; i < len(ar.drawables); i++ {
		ar.drawables[i].Draw2D()
	}
}

func (ar *ARRender) UploadVideoFrame() {
	pixels := ar.videoframe_pixels
	// uploads the image frame to the frame texture
	ar.frametexture.Bind(gl.TEXTURE_2D)
	gl.TexSubImage2D(gl.TEXTURE_RECTANGLE, 0, 0, 0,
		ar.frametexture_width, ar.frametexture_height,
		gl.RGBA, gl.UNSIGNED_BYTE, pixels)

	// map frame texture to FBO / window-default-provided FBO
	if ar.rendermode == ORIGINAL {
		// binding to window-system-provided FrameBuffer
		gl.Framebuffer(0).Bind()
		gl.Viewport(0, 0, ar.Width(), ar.Height())
	} else { // UNDISTORT / DISTORT
		ar.framebuffer.Bind()
		gl.Viewport(0, 0, ar.framebuffer_width, ar.framebuffer_height)
	}
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	gl.Ortho(0, 1, 0, 1, 0, 1) // OpenGL coordinates (start at bottom-left)

	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()

	gl.Enable(gl.TEXTURE_RECTANGLE)
	ar.frametexture.Bind(gl.TEXTURE_2D)
	gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_MIN_FILTER, gl.LINEAR)

	gl.Disable(gl.POLYGON_SMOOTH)
	gl.Disable(gl.BLEND)
	gl.EnableClientState(gl.VERTEX_ARRAY)
	gl.EnableClientState(gl.TEXTURE_COORD_ARRAY)
	if ar.rendermode == ORIGINAL {
		fa_texcoords := []float64{0, 0,
			float64(ar.frametexture_width), 0,
			float64(ar.frametexture_width), float64(ar.frametexture_height),
			0, float64(ar.frametexture_height)}
		fa_vertex := []float64{0, 0, 1, 0, 1, 1, 0, 1}
		gl.TexCoordPointer(2, gl.DOUBLE, 0, fa_texcoords)
		gl.VertexPointer(2, gl.DOUBLE, 0, fa_vertex)
		gl.DrawArrays(gl.QUADS, 0, 4)
	} else { // UNDISTORT / DISTORT
		gl.TexCoordPointer(2, gl.DOUBLE, 64*2, ar.dist_texcoords)
		gl.VertexPointer(2, gl.DOUBLE, 64*2, ar.undist_vertexs)

		gl.DrawArrays(gl.TRIANGLE_STRIP, 0, len(ar.dist_texcoords))
	}
	gl.DisableClientState(gl.VERTEX_ARRAY)
	gl.DisableClientState(gl.TEXTURE_COORD_ARRAY)
	gl.Disable(gl.TEXTURE_RECTANGLE)

	gl.ClearDepth(1)
	gl.Clear(gl.DEPTH_BUFFER_BIT)
}

func (ar *ARRender) MapFBO2SystemFBO() {
	// map FBO Texture to default window-system-provided FBO
	gl.Framebuffer(gl.FRAMEBUFFER_DEFAULT).Bind()
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	gl.Viewport(0, 0, ar.Width(), ar.Height())

	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	gl.Ortho(0, 1, 0, 1, 0, 1) // OpenGL coordinates (start at bottom-left)

	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()

	v2_offset := geometry.MakeVector2d(0.5*float64(ar.framebuffer_width-ar.frametexture_width),
		0.5*float64(ar.framebuffer_height-ar.frametexture_height))
	gl.Enable(gl.TEXTURE_RECTANGLE)
	ar.framebuffer_texture.Bind(gl.TEXTURE_RECTANGLE)
	gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_MIN_FILTER, gl.LINEAR)

	gl.Disable(gl.POLYGON_SMOOTH)
	gl.Disable(gl.BLEND)
	gl.EnableClientState(gl.VERTEX_ARRAY)
	gl.EnableClientState(gl.TEXTURE_COORD_ARRAY)

	if ar.rendermode == UNDISTORT {
		fa_texcoords := []float64{v2_offset.Get(0), v2_offset.Get(1),
			v2_offset.Get(0), v2_offset.Get(1) + float64(ar.frametexture_height),
			v2_offset.Get(0) + float64(ar.frametexture_width), v2_offset.Get(1) + float64(ar.frametexture_height),
			v2_offset.Get(0) + float64(ar.frametexture_width), v2_offset.Get(1)}

		fa_vertex := []float64{0, 0, 0, 1, 1, 1, 1, 0}
		gl.TexCoordPointer(2, gl.DOUBLE, 0, fa_texcoords)
		gl.VertexPointer(2, gl.DOUBLE, 0, fa_vertex)
		gl.DrawArrays(gl.QUADS, 0, 4)
	} else if ar.rendermode == DISTORT {
		gl.TexCoordPointer(2, gl.FLOAT, 32*2, ar.undist_texcoords)
		gl.VertexPointer(2, gl.FLOAT, 32*2, ar.dist_vertexs)
		gl.DrawArrays(gl.TRIANGLE_STRIP, 0, len(ar.dist_texcoords))
	}
	gl.DisableClientState(gl.VERTEX_ARRAY)
	gl.DisableClientState(gl.TEXTURE_COORD_ARRAY)

	gl.Disable(gl.TEXTURE_RECTANGLE)
}

func (ar *ARRender) GenerateTextureMappingCoordinates() {
	// scaled by aspect ratio
	y_step := int(sample_steps * float32(ar.frametexture_height) / float32(ar.frametexture_width))
	if y_step < 2 {
		y_step = 2
	}

	// offset between frame texture (video frame) & frame buffer texture (FBO)
	v2_offset := geometry.MakeVector2d(0.5*float64(ar.framebuffer_width-ar.frametexture_width),
		0.5*float64(ar.framebuffer_height-ar.frametexture_height))

	for y := 0; y < y_step; y++ {
		current_size := len(ar.dist_texcoords)
		for x := 0; x < sample_steps; x++ {
			for yy := y; yy <= y+1; yy++ {
				v2_distorted := geometry.MakeVector2d(0, 0)
				v2_distorted.Set(0, float64(x*ar.frametexture_width)/float64(sample_steps))

				v2_distorted.Set(1, float64(yy*ar.frametexture_height)/float64(y_step))
				if x == 0 || yy == 0 || x == sample_steps || yy == y_step {
					for i := 0; i < 2; i++ {
						v2_distorted.Set(i, v2_distorted.Get(i)*1.02-0.01)
					}
				}
				v2_undistorted := ar.video_camera.ProjectLinear(ar.video_camera.UnProject(v2_distorted))

				if y%2 == 0 { // odd row will start from tail of the previous even row
					current_size = len(ar.dist_texcoords)
				}
				// video frame (vision coordinates) to FBO (OpenGL coordinates)
				t := append(ar.dist_texcoords[:current_size], []float64{v2_distorted.Get(0), v2_distorted.Get(1)}...)
				ar.dist_texcoords = append(t, ar.dist_texcoords[current_size:]...)

				a := (v2_undistorted.Get(0) + v2_offset.Get(0)) / float64(ar.framebuffer_width)
				b := 1 - (v2_undistorted.Get(1)+v2_offset.Get(1))/float64(ar.framebuffer_height)
				t = append(ar.undist_vertexs[:current_size], []float64{a, b}...)
				ar.undist_vertexs = append(t, ar.undist_vertexs[current_size:]...)

				// FBO to default FBO
				a = v2_undistorted.Get(0) + v2_offset.Get(0)
				b = v2_undistorted.Get(1) + v2_offset.Get(1)
				t = append(ar.undist_texcoords[:current_size], []float64{a, b}...)
				ar.undist_texcoords = append(t, ar.undist_texcoords[current_size:]...)

				a = v2_distorted.Get(0) / float64(ar.frametexture_width)
				b = v2_distorted.Get(1) / float64(ar.frametexture_height)
				t = append(ar.dist_vertexs[:current_size], []float64{a, b}...)
				ar.dist_vertexs = append(t, ar.dist_vertexs[current_size:]...)
			}
		}
	}
}

func (ar *ARRender) GenerateFrameBuffers() bool {
	ar.texture_initialized = true
	// make texture function to be in GL_DECAL mode (replacing background colors)
	// http://glprogramming.com/red/chapter09.html
	gl.TexEnvf(gl.TEXTURE_ENV, gl.TEXTURE_ENV_MODE, gl.DECAL)
	// generate frame texture
	ar.frametexture = gl.GenTexture()
	ar.frametexture.Bind(gl.TEXTURE_RECTANGLE)
	gl.TexSubImage2D(gl.TEXTURE_RECTANGLE, 0, gl.RGBA,
		ar.frametexture_width, ar.frametexture_height, 0,
		gl.RGBA, gl.UNSIGNED_BYTE, nil)

	// generate frame buffer texture
	ar.framebuffer_texture = gl.GenTexture()
	ar.framebuffer_texture.Bind(gl.TEXTURE_RECTANGLE)
	gl.TexSubImage2D(gl.TEXTURE_RECTANGLE, 0, gl.RGBA,
		ar.framebuffer_width, ar.framebuffer_height, 0,
		gl.RGBA, gl.UNSIGNED_BYTE, nil)

	gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_MAG_FILTER, gl.NEAREST)

	// generate render buffer (depth buffer)
	ar.framebuffer_depthbuffer = gl.GenRenderbuffer() // TODO: fix this segment fault error ????
	ar.framebuffer_depthbuffer.Bind()
	gl.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT24, ar.framebuffer_width, ar.framebuffer_height)

	// generate frame buffer object
	ar.framebuffer = gl.GenFramebuffer()
	ar.framebuffer.Bind()
	gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_RECTANGLE, ar.framebuffer_texture, 0)

	n := gl.CheckFramebufferStatus(gl.FRAMEBUFFER)
	if n == gl.FRAMEBUFFER_COMPLETE {
		return true // All good
	}
	log.Fatal("ARRender:: gl.CheckFrameBufferStatus returned an error.\n")
	return false
}

func (ar *ARRender) Release() {
	if !ar.released {
		ar.released = true
		gl.DeleteFramebuffers([]gl.Framebuffer{ar.framebuffer})
		gl.DeleteTextures([]gl.Texture{ar.frametexture, ar.framebuffer_texture})
	}
}

// set frustum typical near, far = 0.001, 1.e+6
func (ar *ARRender) set_frustum(z_near float64, z_far float64) {
	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	// figure out right frustum for whole FrameBuffer
	// (larger size than video frame, to cover texture)
	v2 := geometry.MakeVector2d(float64(ar.framebuffer_width-ar.frametexture_width)*0.5,
		float64(ar.framebuffer_height-ar.frametexture_height)*0.5)
	// note: this is in OpenGL coordinates (start bottom-left)
	bl := ar.video_camera.UnProjectLinear(geometry.MakeVector2d(-v2.Get(0), -v2.Get(1)))
	tr := ar.video_camera.UnProjectLinear(
		geometry.MakeVector2d(float64(ar.frametexture_width)+v2.Get(0),
			float64(ar.frametexture_height)+v2.Get(1)))
	left := bl.Get(0) * z_near
	right := tr.Get(0) * z_near
	top := tr.Get(1) * z_near
	bottom := bl.Get(1) * z_near
	gl.Frustum(left, right, bottom, top, z_near, z_far)
}

func (ar *ARRender) Width() int { return ar.currentviewport_width }

func (ar *ARRender) Height() int { return ar.currentviewport_height }

func (ar *ARRender) ResizeWindow(w, h int) {
	ar.currentviewport_width = w
	ar.currentviewport_height = h
	// setup viewport
	gl.Framebuffer(gl.FRAMEBUFFER_DEFAULT).Bind()
	gl.Viewport(0, 0, w, h)
}

func (ar *ARRender) AddDrawable(drawable AbstractDrawable) {
	ar.drawables = append(ar.drawables, drawable)
}

func (ar *ARRender) SetRenderMode(mode int) {
	ar.rendermode = mode
}

func (ar *ARRender) SetVideoFrame(pixels []uint8) {
	ar.videoframe_pixels = pixels
}

func (ar *ARRender) SetCameraPose(pose ar_math.SE3) {
	ar.pose = pose
}

// convert coordinates from imageplane to current viewport coordinates
func (ar *ARRender) ConvertCoordinate(x, y int) {
	x = x * ar.Width() / ar.frametexture_width
	y = y * ar.Height() / ar.frametexture_height
}
