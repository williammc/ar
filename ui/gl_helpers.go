// Copyright 2014 The Authors. All rights reserved.

package ui

import (
	"github.com/go-gl/gl"
	matrix "github.com/skelterjohn/go.matrix"
)

func glMultMatrix(m *matrix.DenseMatrix) {

	// OpenGL storate matrix in Column-major order
	var m_data [16]float64
	m_data[0] = m.Get(0, 0)
	m_data[1] = m.Get(1, 0)
	m_data[2] = m.Get(2, 0)
	m_data[3] = m.Get(3, 0)
	m_data[4] = m.Get(0, 1)
	m_data[5] = m.Get(1, 1)
	m_data[6] = m.Get(2, 1)
	m_data[7] = m.Get(3, 1)
	m_data[8] = m.Get(0, 2)
	m_data[9] = m.Get(1, 2)
	m_data[10] = m.Get(2, 2)
	m_data[11] = m.Get(3, 2)
	m_data[12] = m.Get(0, 3)
	m_data[13] = m.Get(1, 3)
	m_data[14] = m.Get(2, 3)
	m_data[15] = m.Get(3, 3)

	gl.MultMatrixd(&m_data)
}
