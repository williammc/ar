// Copyright 2014 The Authors. All rights reserved.

package ui

import (
	"github.com/go-gl/glfw"
)

type GLWindowCallback interface {
	GetVideoFrame() []uint8
	OnDisplay()
	OnKeyboard(key int)
}

type GLWindow struct {
	window_width  int
	window_height int
	full_screen   bool
	pausing       bool
	stop          bool

	callback GLWindowCallback
	arrender *ARRender
}

func NewGLWindow(arrender *ARRender, callback GLWindowCallback) (w *GLWindow) {
	w = new(GLWindow)
	w.arrender = arrender
	w.callback = callback
	w.full_screen = false
	w.pausing = false
	w.stop = false
	return
}

// Render graphics (video frame & necessary objects)
func (w *GLWindow) Main() {
	defer w.arrender.Release()
	for !w.stop {
		if !w.pausing && glfw.WindowParam(glfw.Opened) == 1 {
			w.callback.OnDisplay()
			if w.arrender != nil {
				w.arrender.SetVideoFrame(w.callback.GetVideoFrame())
				w.arrender.Render()
				glfw.SwapBuffers()
			}
		}
	}
}

// for glut keyboard handling
func (w *GLWindow) OnKeyboard(key int) {
	w.callback.OnKeyboard(key)
	switch key {
	case glfw.KeyEsc:
		w.stop = true
		break
	case 'w':
		w.toggle_full_screen() // full screen or normal screen
		break
	case 'p':
		w.pausing = !w.pausing
		break
	case 'o':
		w.arrender.SetRenderMode(ORIGINAL)
		break
	case 'd':
		w.arrender.SetRenderMode(DISTORT)
		break
	case 'u':
		w.arrender.SetRenderMode(UNDISTORT)
		break
	}
}

func (wi *GLWindow) Reshape(w, h int) {
	if wi.arrender != nil {
		glfw.SetWindowSize(w, h)
		wi.arrender.ResizeWindow(w, h)
		wi.window_width, wi.window_height = w, h
	}
}

func (w *GLWindow) toggle_full_screen() {
	w.full_screen = !w.full_screen
	if !w.full_screen {
		glfw.SetWindowSize(w.window_width, w.window_height)
		w.arrender.ResizeWindow(w.window_width, w.window_height)
	}
}
